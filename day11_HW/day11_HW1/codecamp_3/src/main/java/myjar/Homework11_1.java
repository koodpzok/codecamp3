package myjar;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.type.TypeReference; // พิมพ์ไว้นอก Class
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper; // พิมพ์ไว้นอก Class
import myjar.Employee;
/**
 * Hello world!
 *
 */
public class Homework11_1
{
    public static void main( String[] args )
    {
        Employee[] array;
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            
            array = objectMapper.readValue(new File("target/homework3_1.json"), Employee[].class);
            // System.out.println(array[1].);// Dang
    
             for(int i = 0; i < array.length; i++){
                System.out.println("id: " + array[i].getId());
                System.out.println("firstname: " + array[i].getFirstname());
                System.out.println("lastname: "+ array[i].getLastname());
                System.out.println("company: " + array[i].getCompany());
                System.out.println("salary: " + array[i].getSalary());
             }
         } catch (IOException ex) {
            System.out.println("JSON parse to Employee object error " + ex.getMessage());
         }

        //  try{
        //     objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
            
        //    // Employee[] employees = objectMapper.readValue(array, Employee[].class);
        //   //  System.out.println(employees[1].getFirstname()); // Captain2
        //  } catch (IOException ex) {
        //     System.out.println("JSON parse error " + array + ex.getMessage());
        //  }
         
         
    }
}
