<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>

</head>

<body>

    <table id="myTable" style="border-collapse: collapse">
        <tr>
            <th style="border:1px solid black; text-align: center">Student Id</th>
            <th style="border:1px solid black; text-align: center">First name</th>
            <th style="border:1px solid black; text-align: center">Last name</th>
        </tr>
        <c:forEach items="${Student_Array}" var="element">
            <tr>
                <td style="border:1px solid black; text-align: center">
                    <c:out value="${element.studentId}" />
                </td>
                <td style="border:1px solid black">
                    <c:out value="${element.firstName}" />
                </td>
                <td style="border:1px solid black">
                    <c:out value="${element.lastName}" />
                </td>
            </tr>
        </c:forEach>

    </table>



</body>

</html>