package com.codecamp.web;

//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Book {
    private String ISBN;
    private String title;
    private String price;

    public String getISBN() {
        return this.ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Book() {
        this("90230230", "This is a book", "15");
        // this("XXXXfirstname","XXXXXlastname","Male",444);
    }

    public Book(String ISBN, String title, String price) {
        this.ISBN = ISBN;
        this.title = title;
        this.price = price;

        // this.salary = salary;
        // this.username = username;
        // this.password = password;
        // this.company = company;

    }
    // public Employee(int id, String firstname, String lastname, int salary)
    // {
    // this.id = id;
    // this.firstname = firstname;
    // this.lastname = lastname;
    // this.salary = salary;
    // }
    // public Employee(String firstname, String lastname,String gender, int salary)
    // {
    // this.firstname = firstname;
    // this.lastname = lastname;
    // this.gender = gender;
    // this.salary = salary;
    // }

    // ห้ามใส่ชื่อฟังชั่นขึ้นต้นว่า get เพราะ jackson library จะดึงไปเกี่่ยว
    // public String getFullName(){
    // String fullname = "";
    // if(gender.toLowerCase().equals("male"))
    // fullname += "Mr. "+this.firstname + " " + this.lastname;
    // else if(gender.toLowerCase().equals("female"))
    // fullname += "Mrs. "+this.firstname + " " + this.lastname;

    // return fullname;
    // }

    // public int getDoubleSalary() {
    // return this.salary*2 ;
    // }

}