<!--web config-->
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd">
    <display-name>Review eiei</display-name>

    <servlet>
        <servlet-name>day22Review</servlet-name>
        <servlet-class>
            org.springframework.web.servlet.DispatcherServlet
        </servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet-mapping>
        <servlet-name>day22Review</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

</web-app>