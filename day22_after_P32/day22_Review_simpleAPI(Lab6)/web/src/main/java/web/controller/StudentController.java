package web.controller;

import java.io.FileNotFoundException;

import javax.swing.Spring;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerExceptionResolverComposite;

import web.domain.Student;
import web.exception.SpringException;

@Controller
public class StudentController {
    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public ModelAndView student() {
        // Student a = new Student();
        // a.setName("gge");
        // return new ModelAndView("student", "command", new Student());
        return new ModelAndView("student", "studentZ", new Student());
        // สร้างstudent objectเปล่าๆเก็บไว้ในstudentZ แล้วเอาไปโชว์ในstudent.jsp

    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    // public String addStudent(@ModelAttribute("SpringWeb") Student student,
    // ModelMap model) {
    public String addStudent(@ModelAttribute("studentZ") Student student, ModelMap model) {
        // we already set the student name age id from browser now we r getting it to
        // show on result.jsp

        model.addAttribute("name", student.getNameE());

        if (student.getAge() > 100) {
            throw new SpringException("What is wrong ?");
        } else {
            model.addAttribute("age", student.getAge());
        }

        model.addAttribute("id", student.getId());

        return "result";

    }

    // @RequestMapping(value = "/register", method = RequestMethod.GET)
    // public String goToException(ModelMap model) {

    // try {
    // return "zzz";
    // }
    // catch(Exception ex) {
    // return new SpringException("NOTHING is found");
    // }

    // }
}
