package web.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import web.exception.SpringException;

//dispatcherServlet config
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "web" })
public class day22Review_servlet implements WebMvcConfigurer {
  @Bean
  public InternalResourceViewResolver resolver() {
    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
    resolver.setViewClass(JstlView.class);
    resolver.setPrefix("/");
    resolver.setSuffix(".jsp");
    return resolver;
  }

  @Bean
  public HandlerExceptionResolver errorHandler() {
    SimpleMappingExceptionResolver s = new SimpleMappingExceptionResolver();
    Properties p = new Properties();
    p.setProperty(SpringException.class.getName(), "springExceptionView");
    // SpringException.class.getName() มี valueเป็น springExceptionView.jsp
    s.setExceptionMappings(p);
    s.setDefaultStatusCode(400);
    return s;
  }

}
