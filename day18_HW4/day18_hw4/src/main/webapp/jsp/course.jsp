<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>

<head>

</head>

<body>


    <table id="myTable" style="border-collapse: collapse;">
        <tr>
            <th style="border:1px solid black; text-align: center">course id</th>
            <th style="border:1px solid black; text-align: center">course name</th>

        </tr>
        <c:forEach items="${HWClass_Array}" var="element">
            <tr>
                <td style="border:1px solid black; text-align: center">
                    <c:out value="${element.getCourseId()}" />
                </td>
                <td style="border:1px solid black; text-align: center">
                    <a href="course/${element.getCourseName()}">
                        <c:out value="${element.getCourseName()}" /></a>
                </td>

            </tr>
        </c:forEach>

    </table>


</body>

</html>