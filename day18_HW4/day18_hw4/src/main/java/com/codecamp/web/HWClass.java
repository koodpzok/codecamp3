package com.codecamp.web;

/**
 * HWClass
 */
public class HWClass {

    private int courseId;
    private String courseName;
    private int price;
    private int instructorId;
    private String instructorName;
    private int studentId;
    private String studentName;

    public HWClass() {

    }

    // Setters
    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setInstructorId(int instructorId) {
        this.instructorId = instructorId;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    // Getters
    public int getCourseId() {
        return this.courseId;
    }

    public String getCourseName() {
        return this.courseName;
    }

    public int getPrice() {
        return this.price;
    }

    public int getInstructorId() {
        return this.instructorId;
    }

    public String getInstructorName() {
        return this.instructorName;
    }

    public int getStudentId() {
        return this.studentId;
    }

    public String getStudentName() {
        return this.studentName;
    }

}