package com.codecamp.web;

import com.codecamp.web.Enrolls;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class TotalCostServlet extends HttpServlet {
    // final String KEY_PAGE_VISIT = "pageVisit";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "lnwsuper");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select enrolls.student_id,enrolls.course_id,courses.price from enrolls inner join courses on enrolls.course_id = courses.id order by student_id asc;");
            // System.out.println(resultSet);
            ArrayList<Enrolls> enroll_arr = new ArrayList<Enrolls>();

            while (resultSet.next()) {
                Enrolls enroll_obj = new Enrolls();
                String student_id = resultSet.getString("student_id");
                String course_id = resultSet.getString("course_id");
                String price = resultSet.getString("price");
                // String lastname = resultSet.getString("lastname");
                // String age = resultSet.getString("age");

                enroll_obj.setStudent_id(student_id);
                enroll_obj.setCourse_id(course_id);
                enroll_obj.setPrice(price);
                // person.setId(id);
                // person.setName(name);
                enroll_arr.add(enroll_obj);
                // persons.add(person);
                // System.out.println("ID=" + id + ", name=" + name);
                // System.out.println(String.format("ID=%s, Firstname=%s, Lastname=%s",
                // studentId, firstname, lastname));

            }

            req.setAttribute("Enroll_array", enroll_arr);
            // System.out.println(students.size());
            req.getRequestDispatcher("/jsp/total-cost.jsp").forward(req, resp);
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
