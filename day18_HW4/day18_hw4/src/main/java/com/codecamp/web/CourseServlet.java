package com.codecamp.web;

import com.codecamp.web.Enrolls;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class CourseServlet extends HttpServlet {
    // final String KEY_PAGE_VISIT = "pageVisit";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {

            String URL_C = req.getRequestURI();
            // System.out.println(URLEncoder.encode("Writing #2", "UTF-8").replace("+",
            // "%20").replace("%23", "#"));
            if (URL_C.equals("/course") || URL_C.equals("/course/")) {
                DriverManager.registerDriver(new Driver());
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root",
                        "lnwsuper");
                Statement statement = connection.createStatement();

                ResultSet resultSet = statement
                        .executeQuery("select id as course_id ,name as course_name from courses order by id;");
                // System.out.println(resultSet);
                ArrayList<HWClass> HWClassArray = new ArrayList<HWClass>();
                // String sumPrice = "";
                while (resultSet.next()) {
                    HWClass hwclass_Obj = new HWClass();

                    int course_id = resultSet.getInt("course_id");
                    String course_name = resultSet.getString("course_name");

                    hwclass_Obj.setCourseId(course_id);
                    hwclass_Obj.setCourseName(course_name);

                    HWClassArray.add(hwclass_Obj);
                    // HWClass hwclass_Obj = new HWClass();

                    // int course_id = resultSet.getInt("course_id");
                    // String course_name = resultSet.getString("course_name");
                    // int price = resultSet.getInt("price");
                    // int instructor_id = resultSet.getInt("instructor_id");
                    // String instructor_name = resultSet.getString("instructor_name");
                    // int student_id = resultSet.getInt("student_id");
                    // String student_name = resultSet.getString("student_name");

                    // hwclass_Obj.setCourseId(course_id);
                    // hwclass_Obj.setCourseName(course_name);
                    // hwclass_Obj.setPrice(price);
                    // hwclass_Obj.setInstructorId(instructor_id);
                    // hwclass_Obj.setInstructorName(instructor_name);
                    // hwclass_Obj.setStudentId(student_id);
                    // hwclass_Obj.setStudentName(student_name);

                    // HWClassArray.add(hwclass_Obj);

                }
                req.setAttribute("switch", "none");
                req.setAttribute("HWClass_Array", HWClassArray);
                // System.out.println(students.size());
                req.getRequestDispatcher("/jsp/course.jsp").forward(req, resp);

                statement.close();
                connection.close();
            } else {
                // String URL_C = URLEncoder.encode("Writing #2", "UTF-8").replace("+",
                // "%20").replace("%23", "#");
                DriverManager.registerDriver(new Driver());
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root",
                        "lnwsuper");
                Statement statement = connection.createStatement();
                ResultSet resultSet = null;// initialize
                ArrayList<HWClass> HWClassArray = new ArrayList<HWClass>();

                switch (URL_C) {

                case "/course/Cooking":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Cooking';");

                    break;
                case "/course/Acting":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Acting';");

                    break;
                case "/course/Chess":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Chess';");

                    break;
                case "/course/Writing":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Writing';");
                    break;
                case "/course/Conservation":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Conservation';");

                    break;
                case "/course/Tennis":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Tennis';");

                    break;
                case "/course/The%20Art%20of%20Performance":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'The Art of Performance';");

                    break;
                case "/course/Writing%20":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Writing #2';");
                    break;
                case "/course/Building%20a%20Fashion%20Brand":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Building a Fashion Brand';");

                    break;
                case "/course/Design%20and%20Architecture":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Design and Architecture';");

                    break;
                case "/course/Singing":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Singing';");
                    break;
                case "/course/Jazz":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Jazz';");
                    break;
                case "/course/Country%20Music":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Country Music';");

                    break;
                case "/course/Fashion%20Design":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Fashion Design';");

                    break;
                case "/course/Film%20Scoring":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Film Scoring';");
                    break;
                case "/course/Comedy":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Comedy';");

                    break;
                case "/course/Writing%20for%20Television":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Writing for Television';");

                    break;
                case "/course/Filmmaking":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Filmmaking';");

                    break;
                case "/course/Dramatic%20Writing":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Dramatic Writing';");

                    break;
                case "/course/Screenwriting":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Screenwriting';");

                    break;
                case "/course/Electronic%20Music%20Production":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Electronic Music Production';");

                    break;
                case "/course/Cooking%20":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Cooking #2';");

                    break;
                case "/course/Shooting,%20Ball%20Handler,%20and%20Scoring":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Shooting, Ball Handler, and Scoring';");

                    break;
                case "/course/Photography":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Photography';");
                    break;
                case "/course/Database%20System%20Concept":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'Database System Concept';");

                    break;
                case "/course/JavaScript%20for%20Beginner":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'JavaScript for Beginner';");

                    break;
                case "/course/OWASP%20Top%2010":
                    resultSet = statement.executeQuery(
                            "select courses.name as course_name, courses.price, instructors.name as instructor_name, students.name as student_name from courses left join instructors on courses.teach_by = instructors.id left join enrolls on courses.id = enrolls.course_id left join students on enrolls.student_id = students.id where courses.name = 'OWASP Top 10';");

                    break;

                }

                while (resultSet.next()) {
                    HWClass hwclass_Obj = new HWClass();

                    String course_name = resultSet.getString("course_name");
                    int price = resultSet.getInt("price");
                    String instructor_name = resultSet.getString("instructor_name");
                    String student_name = resultSet.getString("student_name");

                    hwclass_Obj.setCourseName(course_name);
                    hwclass_Obj.setPrice(price);
                    hwclass_Obj.setInstructorName(instructor_name);
                    hwclass_Obj.setStudentName(student_name);

                    HWClassArray.add(hwclass_Obj);
                }
                req.setAttribute("HWClass_Array", HWClassArray);

                req.getRequestDispatcher("/jsp/HWClassJSP.jsp").forward(req, resp);
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
