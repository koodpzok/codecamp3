package com.codecamp.web;

public class Person {
    private String id;
    private String name;

    public Person() {

    }

    // Getter
    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    // Setter
    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}