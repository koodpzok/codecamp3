package com.codecamp.web;

import com.codecamp.web.Employee;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class HomeServlet extends HttpServlet {
    // final String KEY_PAGE_VISIT = "pageVisit";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root",
                    "lnwsuper");
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("SELECT * FROM employee");

            // System.out.println(resultSet);
            ArrayList<Employee> employees = new ArrayList<Employee>();
            while (resultSet.next()) {
                Employee employee = new Employee();
                String id = resultSet.getString("id");
                String firstname = resultSet.getString("firstname");
                String lastname = resultSet.getString("lastname");
                String age = resultSet.getString("age");

                employee.setId(id);
                employee.setFirstname(firstname);
                employee.setLastname(lastname);
                employee.setAge(age);

                employees.add(employee);
                System.out.println("ID=" + id + ", Firstname=" + firstname + ", Lastname=" + lastname + ", Age=" + age);
                // System.out.println(String.format("ID=%s, Firstname=%s, Lastname=%s",
                // studentId, firstname, lastname));
            }

            // from table book
            ResultSet resultSet_book = statement.executeQuery("SELECT * FROM book");
            // System.out.println(resultSet);
            ArrayList<Book> books = new ArrayList<Book>();
            while (resultSet_book.next()) {
                Book book = new Book();
                String ISBN = resultSet_book.getString("ISBN");
                String title = resultSet_book.getString("title");
                String price = resultSet_book.getString("price");
                // String age = resultSet_book.getString("age");

                book.setISBN(ISBN);
                book.setTitle(title);
                book.setPrice(price);

                books.add(book);
                System.out.println("ISBN=" + ISBN + ", Tile=" + title + ", Price=" + price);
                // System.out.println(String.format("ID=%s, Firstname=%s, Lastname=%s",
                // studentId, firstname, lastname));
            }

            req.setAttribute("Employee_Array", employees);
            req.setAttribute("Book_Array", books);
            // System.out.println(students.size());
            req.getRequestDispatcher("/jsp/home.jsp").forward(req, resp);
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
