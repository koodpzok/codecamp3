package com.codecamp.web;

import com.codecamp.web.Employee;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.fasterxml.jackson.core.type.TypeReference; // พิมพ์ไว้นอก Class
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper; // พิมพ์ไว้นอก Class

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.cj.jdbc.Driver;

public class LoginServlet extends HttpServlet {
    // final String KEY_PAGE_VISIT = "pageVisit";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // String username = req.getParameter("username");
        // Cookie cookie = new Cookie("username", username);

        // resp.addCookie(cookie);
        // If()
        String username = "";
        String password = "";
        String firstname = "";
        String lastname = "";
        // InputStream stream =
        // this.getServletContext().getResourceAsStream("/WEB-INF/single_employee.json");
        // BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        // StringBuilder stringBuilder = new StringBuilder();
        // String line;
        // while ((line = reader.readLine()) != null) {
        // stringBuilder.append(line);
        // }
        // reader.close();

        // String jsonContent = stringBuilder.toString();
        // ObjectMapper objectMapper = new ObjectMapper();
        // use jackson library
        try {

            // System.out.println(map.get("firstname")); // Captain
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/codecamp3_employee",
                    "root", "lnwsuper");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM user");
            ArrayList<Employee> students = new ArrayList<Employee>();

            if (resultSet.next()) {
                // Employee student = new Employee();
                username = resultSet.getString("username");
                password = resultSet.getString("password");
                firstname = resultSet.getString("firstname");
                lastname = resultSet.getString("lastname");
                HttpSession session = req.getSession();// ข้างบนกับข่้างล่างคือsessionเดียวกัน

                if (((req.getParameter("username").equals(username))
                        && (req.getParameter("password").equals(password)))) {

                    session.setMaxInactiveInterval(30);

                    // all these three variables will be added to sessionScope variable
                    session.setAttribute("firstname", firstname);
                    session.setAttribute("lastname", lastname);
                    session.setAttribute("loginmessage", "You have signed-in as " + firstname + " " + lastname);
                    // req.getRequestDispatcher("/jsp/member.jsp").forward(req, resp);
                    resp.sendRedirect("member");//

                } else {

                    session.setAttribute("message", "username/password is incorrect.");
                    // req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);// เอาattribute
                    // ส่งไปหน้าถัดไป

                    resp.sendRedirect("login");//
                }

                // System.out.println(String.format("ID=%s, Firstname=%s, Lastname=%s",
                // studentId, firstname, lastname));
            }
            // username = (String) map.get("username");
            // password = (String) map.get("password");
            // firstname = (String) map.get("firstname");
            // lastname = (String) map.get("lastname");
            // req.setAttribute("usernamee", map.get("username"));
            // req.setAttribute("password", map.get("password"));
            // if (req.getAttribute("usernamee").equals("dang"))
            // resp.sendRedirect("member");//
            // Cookie c = new Cookie("candy","mlml");
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            // System.out.println("JSON parse error :" + jsonContent + ex.getMessage());
            // req.setAttribute("name", jsonContent);
        }

        // req.setAttribute("message", "");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        // req.setAttribute("name", jsonContent);
        req.setAttribute("message", session.getAttribute("message")); // assign attribute จาก session เข้าไปเก็บในreq
                                                                      // ก่อนลบทิ้ง ครั้งแรกที่เรียกgetจะมีmsg
                                                                      // ครั้งสองที่เรียกgetจะไม่มี
        if (session.getAttribute("message") != null)
            session.removeAttribute("message");

        req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
        // resp.sendRedirect("login");//
    }

}
