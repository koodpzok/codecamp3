package com.codecamp.web;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import com.codecamp.web.Student;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class MemberServlet extends HttpServlet {
    // final String KEY_PAGE_VISIT = "pageVisit";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // HttpSession session = req.getSession();
        // // session will expire in 60 seconds
        // session.setMaxInactiveInterval(60);

        // Integer pageVisit = (Integer) session.getAttribute(KEY_PAGE_VISIT);

        // if (pageVisit == null)
        // pageVisit = 0;

        // session.setAttribute(KEY_PAGE_VISIT, pageVisit + 1);

        //

        // req.setAttribute("Name", "Srinong");

        req.getRequestDispatcher("/jsp/member.jsp").forward(req, resp);

    }

}
