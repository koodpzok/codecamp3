package com.codecamp.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee {
    private int id;
    private String firstname;
    private String lastname;
    private int salary;
    private String username;
    private String password;
    private String company;
    // private String gender;

    public Employee() {
        this(0, "Anonymous", "Anonymous", 0, "koodpzok", "dear123", "WADS");
        // this("XXXXfirstname","XXXXXlastname","Male",444);
    }

    public Employee(int id, String firstname, String lastname, int salary, String username, String password,
            String company) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
        this.username = username;
        this.password = password;
        this.company = company;

    }
    // public Employee(int id, String firstname, String lastname, int salary)
    // {
    // this.id = id;
    // this.firstname = firstname;
    // this.lastname = lastname;
    // this.salary = salary;
    // }
    // public Employee(String firstname, String lastname,String gender, int salary)
    // {
    // this.firstname = firstname;
    // this.lastname = lastname;
    // this.gender = gender;
    // this.salary = salary;
    // }

    // Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    // Getters
    public int getId() {
        return this.id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public int getSalary() {
        return this.salary;
    }

    public String getUserName() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getCompany() {
        return this.company;
    }

    // ห้ามใส่ชื่อฟังชั่นขึ้นต้นว่า get เพราะ jackson library จะดึงไปเกี่่ยว
    // public String getFullName(){
    // String fullname = "";
    // if(gender.toLowerCase().equals("male"))
    // fullname += "Mr. "+this.firstname + " " + this.lastname;
    // else if(gender.toLowerCase().equals("female"))
    // fullname += "Mrs. "+this.firstname + " " + this.lastname;

    // return fullname;
    // }

    // public int getDoubleSalary() {
    // return this.salary*2 ;
    // }

}