package com.codecampthailand.camp3.java.advanced.day2.p004_autowiring_by_constructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component

public class Application {
    private ApplicationUser user;

    @Autowired
    public Application(ApplicationUser user) {
        this.user = user;
    }

    public ApplicationUser getUser() {
        return user;
    }

}