package com.codecampthailand.camp3.java.advanced.day2.domain;

public enum PriceRange {

    LOW("500,000 - 1,000,000"), 
    MIDDLE("1,000,001 - 5,000,000"),
    HIGH("5,000,001 - 10,000,000"),
    SUPER("> 10,000,001");

    private String description = null;

    private PriceRange(String description){
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

}