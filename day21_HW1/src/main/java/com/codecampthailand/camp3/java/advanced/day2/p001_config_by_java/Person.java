package com.codecampthailand.camp3.java.advanced.day2.p001_config_by_java;

import org.springframework.stereotype.Component;

/**
 * Person
 */

public class Person {

    private String fullName;
    private int age;
    private String gender;

    public void init() {
        System.out.println("How do you do?");
    }

    public void close() {
        System.out.println("Good bye!!");
    }

    public Person() {

    }

    // Setters
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    // Getters
    public String getFullName() {
        return this.fullName;
    }

    public int getAge() {
        return this.age;
    }

    public String getGender() {
        return this.gender;
    }

}