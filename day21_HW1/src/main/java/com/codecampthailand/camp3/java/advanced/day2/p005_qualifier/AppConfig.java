package com.codecampthailand.camp3.java.advanced.day2.p005_qualifier;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.codecampthailand.camp3.java.advanced.day2.p005_qualifier")

public class AppConfig {
}