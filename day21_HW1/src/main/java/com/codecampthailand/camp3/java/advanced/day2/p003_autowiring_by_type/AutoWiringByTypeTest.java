package com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AutoWiringByTypeTest {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
			Application application = context.getBean(Application.class);
			// ApplicationUser appUser = context.getBean(ApplicationUser.class);
			// System.out.println(application.getUser());
		}

	}

}