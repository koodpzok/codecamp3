package com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.legacy;

public class MyApplication {

	private EmailService email = new EmailService();

	public void processMessages(String msg, String rec) {
		this.email.sendEmail(msg, rec);
	}

}