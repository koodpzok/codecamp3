package com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.legacy;

public class EmailService {

	public void sendEmail(String message, String receiver) {
		System.out.println("Email sent to " + receiver + " with Message=" + message);
	}
	
}