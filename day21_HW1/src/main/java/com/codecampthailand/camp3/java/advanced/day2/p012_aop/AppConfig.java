package com.codecampthailand.camp3.java.advanced.day2.p012_aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("com.codecampthailand.camp3.java.advanced.day2.p012_aop")
public class AppConfig {
}