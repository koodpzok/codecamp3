package com.codecampthailand.camp3.java.advanced.day2.p005_qualifier;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class QualifierTest {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
			MazdaDealer dealer = (MazdaDealer) context.getBean("MazdaDealer");
			dealer.showCar();
		}

	}

}