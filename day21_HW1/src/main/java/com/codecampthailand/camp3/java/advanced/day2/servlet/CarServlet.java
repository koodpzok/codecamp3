package com.codecampthailand.camp3.java.advanced.day2.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.codecampthailand.camp3.java.advanced.day2.domain.Car;
import com.codecampthailand.camp3.java.advanced.day2.domain.PriceRange;
import com.codecampthailand.camp3.java.advanced.day2.service.CarService;

public class CarServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String priceRange = request.getParameter("priceRange");
		CarService carService = new CarService();
		try {
			// List<Car> cars = carService.getCars(PriceRange.valueOf(priceRange));
			List<Car> cars = new ArrayList<>();
			request.setAttribute("cars", cars);
			response.setHeader("status", "success");
			RequestDispatcher dispatcher = request.getRequestDispatcher("result.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			response.sendRedirect("index.jsp");
		}
	}

}