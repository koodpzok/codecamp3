package com.codecampthailand.camp3.java.advanced.day2.p005_qualifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("MazdaDealer")
public class MazdaDealer {
    @Autowired
    @Qualifier("Mazda")
    Car car;

    public void showCar() {
        System.out.println(car.getCarName());
    }

}