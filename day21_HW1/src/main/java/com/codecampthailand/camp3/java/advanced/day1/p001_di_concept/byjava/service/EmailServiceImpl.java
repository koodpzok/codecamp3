package com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.service;

public class EmailServiceImpl implements MessageService {

	@Override
	public void sendMessage(String msg, String rec) {
		System.out.println("Email sent to " + rec + " with Message=" + msg);
	}

}