package com.codecampthailand.camp3.java.advanced.day21_hw1;

import java.math.BigDecimal;
import java.sql.Connection;
import com.mysql.cj.jdbc.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class day21_HW1 {

    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
            Person pObj = context.getBean(Person.class);
            // MyDb myDb = MyDb.getInstance();
            // Person pObj2 = context.getBean(Person.class);
            // System.out.println(pObj);
            // System.out.println(pObj2);
            // pObj2.getBankAccount().setAccount_no("11111");
            // System.out.println(pObj2.getBankAccount().getAccount_no());
            pObj.getBankAccount().setAccount_no("1001");
            pObj.getBankAccount().setActive(1);
            pObj.getBankAccount().setAmount(BigDecimal.valueOf(5000));
            try {
                DriverManager.registerDriver(new Driver());
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/day21_hw1", "root",
                        "lnwsuper");

                PreparedStatement statement = connection
                        .prepareStatement("INSERT INTO bank_account (account_no,active,amount)" + "VALUES ("
                                + pObj.getBankAccount().getAccount_no() + "," + pObj.getBankAccount().getActive() + ","
                                + pObj.getBankAccount().getAmount() + ");");
                statement.executeUpdate();

                Statement statements = connection.createStatement();

                // statement.executeUpdate("INSERT INTO bank_account (account_no,active,amount)"
                // + "VALUES ("
                // + pObj.getBankAccount().getAccount_no() + "," +
                // pObj.getBankAccount().getActive() + ","
                // + pObj.getBankAccount().getAmount() + ");");

                // DepositMoney
                ResultSet resultSet = statements.executeQuery("select amount from bank_account where account_no=1001;");
                int init_amount = 0;
                if (resultSet.next()) {
                    init_amount = resultSet.getInt("amount");
                }
                int topUp = 500;
                pObj.getBankAccount().setAmount(BigDecimal.valueOf(AddMoney(init_amount, topUp)));

                statements.executeUpdate("UPDATE bank_account SET amount=" + pObj.getBankAccount().getAmount()
                        + " where account_no=1001;");

                statements.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }

    public static int AddMoney(int initialAmount, int topUp) {
        return initialAmount + topUp;
    }

}