package com.codecampthailand.camp3.java.advanced.day2.p002_autowiring_by_name;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

@Component("application")
public class Application {
    @Resource(name = "ApplicationUser") // อ้างถึง component ที่มีชื่อว่า ApplicationUser
    private ApplicationUser user;

    public ApplicationUser getUser() {
        return user;
    }

    public void setUser(ApplicationUser user) {
        this.user = user;
    }

}