package com.codecampthailand.camp3.java.advanced.day2.p005_qualifier;

import org.springframework.stereotype.Component;

@Component("BMW")
public class BMW implements Car {
    @Override
    public String getCarName() {
        return "BMW";
    }

}