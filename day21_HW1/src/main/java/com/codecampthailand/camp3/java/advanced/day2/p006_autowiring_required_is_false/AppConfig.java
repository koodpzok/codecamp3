package com.codecampthailand.camp3.java.advanced.day2.p006_autowiring_required_is_false;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.codecampthailand.camp3.java.advanced.day2.p006_autowiring_required_is_false")

public class AppConfig {
}