package com.codecampthailand.camp3.java.advanced.day2.p002_autowiring_by_name;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AutoWiringByNameTest {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
			Application application = (Application) context.getBean("application");
			System.out.println(application.getUser());
		}

	}

}