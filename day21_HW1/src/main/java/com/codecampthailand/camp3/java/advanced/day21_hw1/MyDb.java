package com.codecampthailand.camp3.java.advanced.day21_hw1;

import com.mysql.jdbc.Driver;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

// @Component
public class MyDb {
    private Connection connection;
    private static MyDb instance = null;

    public static MyDb getInstance() {
        if (instance == null)
            instance = new MyDb("localhost:3306", "root", "lnwsuper", "design_pattern"); // Default Database

        return instance;
    }

    public MyDb(String host, String username, String password, String database) { // Custom Database
        try {
            DriverManager.registerDriver(new Driver());
            connection = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database, username, password);
        } catch (SQLException ex) {
            System.out.println("Connection error: " + ex.toString());
        }
    }

    // public ArrayList<Person> personQueryGet(String sql, String[] bindValues)
    // {
    // ArrayList<Person> allPersons = new ArrayList<>();
    // try {
    // PreparedStatement statement = connection.prepareStatement(sql);

    // for (int i = 0; i < bindValues.length; i++) {
    // statement.setString(i + 1, bindValues[i]);
    // }

    // ResultSet resultSet = statement.executeQuery();
    // while (resultSet.next()) {
    // Person person = new Person(resultSet);
    // allPersons.add(person);
    // }
    // statement.close();
    // } catch (SQLException ex) {
    // System.out.println("Query Error : " + sql);
    // System.out.println("Exception: " + ex.toString());
    // }

    // return allEmployees;
    // }

    // public ArrayList<Instructor> instructorQueryGet(String sql, String[]
    // bindValues) {
    // ArrayList<Instructor> allInstructors = new ArrayList<>();
    // try {
    // PreparedStatement statement = connection.prepareStatement(sql);

    // for (int i = 0; i < bindValues.length; i++) {
    // statement.setString(i + 1, bindValues[i]);
    // }

    // ResultSet resultSet = statement.executeQuery();
    // while (resultSet.next()) {
    // Instructor instructor = new Instructor(resultSet);
    // allInstructors.add(instructor);
    // }
    // statement.close();
    // } catch (SQLException ex) {
    // System.out.println("Query Error : " + sql);
    // System.out.println("Exception: " + ex.toString());
    // }

    // return allInstructors;
    // }

    // public ArrayList<Course> courseQueryGet(String sql, String[] bindValues) {
    // ArrayList<Course> allCourses = new ArrayList<>();
    // try {
    // PreparedStatement statement = connection.prepareStatement(sql);

    // for (int i = 0; i < bindValues.length; i++) {
    // statement.setString(i + 1, bindValues[i]);
    // }

    // ResultSet resultSet = statement.executeQuery();
    // while (resultSet.next()) {
    // Course course = new Course(resultSet);
    // allCourses.add(course);
    // }
    // statement.close();
    // } catch (SQLException ex) {
    // System.out.println("Query Error : " + sql);
    // System.out.println("Exception: " + ex.toString());
    // }

    // return allCourses;
    // }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException ex) {
            System.out.println("Cannot Close Connection" + ex.toString());
        }
    }

}