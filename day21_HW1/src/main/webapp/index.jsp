<%@ page import="com.codecampthailand.camp3.java.advanced.day2.domain.PriceRange" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Car You Can Purchase</title>
</head>
<body>
<div style="text-align:center">
    <h4>What cars are suitable to your money</h4>
    <form name="carForm" action="suggestCar" method="POST">
    <table>
        <tr>
            <td>How much you have in your pocket now? :</td>
            <td>
            	<select name="priceRange" size="1">
            	<%  for (PriceRange item : PriceRange.values()) { %>
					<option value="<%= item %>"><%= item.getDescription() %></option>
				<% 	} %>
            	</select>
            </td>
            <td><input type="submit" value="Submit" name="find"/></td>
        </tr>
    </table>
</form>
</div>
</body>
</html>