package web.domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Spring;

import com.mysql.cj.jdbc.Driver;

import org.springframework.beans.factory.annotation.Autowired;

import web.exception.SpringException;

// import java.util.List;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Component;

/**
 * Person
 */
// @Component
public class Person {

    private String firstName;
    private String lastName;
    private String email;

    public Person() {

    }

    public Person(ResultSet resultSet) {
        try {
            this.firstName = resultSet.getString("firstname");
            this.lastName = resultSet.getString("lastname");
            this.email = resultSet.getString("email");
        } catch (SQLException ex) {
            System.out.println("Cannot new Person Object. " + ex.toString());
        }
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Person> personQueryGet(Connection connection, String sql) {
        List<Person> allPersons = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);

            // for (int i = 0; i < bindValues.length; i++) {
            // statement.setString(i + 1, bindValues[i]);
            // }

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Person person = new Person(resultSet);
                allPersons.add(person);
            }

            statement.close();
        } catch (SQLException ex) {
            System.out.println("Query Error : " + sql);
            System.out.println("Exception: " + ex.toString());
        }

        return allPersons;
    }

    public int personInsertUpdateInDatabase(Connection connection, String sql, String firstname, String lastname,
            String email) {
        int a = 0;
        try {
            // DriverManager.registerDriver(new Driver());
            // connection =
            // DriverManager.getConnection("jdbc:mysql://localhost:3306/day22_hw2", "root",
            // "lnwsuper");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, firstname);
            statement.setString(2, lastname);
            statement.setString(3, email);
            a = statement.executeUpdate();
            // Statement statement = connection.createStatement();
            // statement.executeUpdate(sql);
            // connection.commit();
            statement.close();
            // return 1;
        } catch (SQLException ex) {

            System.out.println("Query Error : " + sql);
            System.out.println("Exception: " + ex.toString());
            throw new SpringException(ex.toString());
        }
        return a;

    }

    // ดูทุก record ใน PersonData
    public List<Person> personGetAll(Connection connection) {
        String sql = "SELECT * FROM persondata;";
        // String[] bindValues = new String[] {};
        // Use dbConnection
        List<Person> Person = personQueryGet(connection, sql);

        return Person;
    }

    // insert ลง table database
    public int personInsert(Connection connection, String firstname, String lastname, String email) {
        String sql = "INSERT INTO persondata (firstname,lastname,email) VALUES (?,?,?);";
        int a = personInsertUpdateInDatabase(connection, sql, firstname, lastname, email);
        return a;
    }

    // public int personUpdate(Connection connection, String firstname, String
    // lastname, String email) {
    // String sql = "UPDATE persondata set firstname = " + firstname + " where
    // firstname = 'dear'";
    // int a = personInsertUpdateInDatabase(connection, sql);
    // return a;
    // }

}