<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
    <title>mimi</title>
    <!-- <link rel="stylesheet" href="style/style.css" /> -->
    <style>
        h2 {
            color:red;
        }
    </style>
</head>
<body>
        <!-- <h2>Student = ${student}</h2>
        <h2>Result = ${result}</h2>
        <h2>Firstname : ${student.getFirstName()}</h2>
      <h2>Lastname : ${student.getLastName()}</h2> -->
      <h2> ${students[1].getFirstName()} </h2>
      <h2> ${students[1].getLastName()} </h2>

      <table border = "1">
            <tr>
               <th>First Name</th>
               <th>Last Name</th>
            </tr>
            <c:forEach var = "item" items = "${students}">
               <tr>
                  <td><c:out value = "${item.getFirstName()}"/></td>
                  <td><c:out value = "${item.getLastName()}"/></td>
               </tr>
            </c:forEach>
         </table>
    
        
</body>
</html>
