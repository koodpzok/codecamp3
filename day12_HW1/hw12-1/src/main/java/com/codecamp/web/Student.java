package  com.codecamp.web;

public class Student {
    private String firstname;
    private String lastname;
    public Student() {
        
    }
    public Student(String firstname, String lastname){
        this.firstname = firstname;
        this.lastname = lastname;
    }

    //Getter
    public String getFirstName() {
        return this.firstname;
    }

    public String getLastName() {
        return this.lastname;
    }

    //Setter
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}