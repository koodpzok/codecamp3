package com.codecampthailand.camp3.java.advanced.day1.domain;

public class EmployeeAddress {

    private String street;
    private String city;

    // @Override
    // public String toString() {
    // return "EmployeeAddress [Street=" + street + ", city=" + city + "]";
    // }

    public String hh() {
        return "hello";
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}