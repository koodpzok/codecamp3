package com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.injector;

import com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.app.MyDIApplication;
import com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.consumer.Consumer;
import com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.service.EmailServiceImpl;

public class EmailServiceInjector implements MessageServiceInjector {

	@Override
	public Consumer getConsumer() {
		return new MyDIApplication(new EmailServiceImpl());
	}

}