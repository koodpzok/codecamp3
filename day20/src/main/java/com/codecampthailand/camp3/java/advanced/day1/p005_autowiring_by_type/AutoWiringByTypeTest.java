package com.codecampthailand.camp3.java.advanced.day1.p005_autowiring_by_type;

import com.codecampthailand.camp3.java.advanced.day1.domain.Employee;
import com.codecampthailand.camp3.java.advanced.day1.domain.EmployeeAddress;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AutoWiringByTypeTest {

	public static void main(String[] args) {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans4.xml")) {
			Employee employee = (Employee) context.getBean("employee");
			System.out.println(employee.getAddress());
			// EmployeeAddress empAdd = (EmployeeAddress)
			// context.getBean("employeeAddress");
			// System.out.println(empAdd.toString());
		}

	}

}