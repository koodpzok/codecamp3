package com.codecampthailand.camp3.java.advanced.day1.p003_di_by_constructor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DIByConstructorTest {

	public static void main(String[] args) {
		// load xml config
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans2.xml")) {
			MyService service = (MyService) context.getBean("myService");// call id 'myService' from beans2.xml
			service.saySomeThing();
			System.out.println(service.getId());
			MyUtil myutilObj = (MyUtil) context.getBean("myUtil"); // Spring สร้าง bean obj ให้เรา
			myutilObj.saySomething("gettttBBBB");

		}

	}

}