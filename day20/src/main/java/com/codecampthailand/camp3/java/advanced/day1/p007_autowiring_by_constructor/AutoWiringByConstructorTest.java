package com.codecampthailand.camp3.java.advanced.day1.p007_autowiring_by_constructor;

import com.codecampthailand.camp3.java.advanced.day1.domain.Musician;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AutoWiringByConstructorTest {

	public static void main(String[] args) {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans6.xml")) {
			Musician musician = (Musician) context.getBean("musician");
			System.out.println(musician.getCar().getName());
		}
	}

}