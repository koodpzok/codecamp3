package com.codecampthailand.camp3.java.advanced.day1.domain;

public class Animal {
    private String name;
    private String animal;
    private String shop;

    public Animal() {

    }

    // setters
    public void setName(String name) {
        this.name = name;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    // getters
    public String getName() {
        return this.name;
    }

    public String getAnimal() {
        return this.animal;
    }

    public String getShop() {
        return this.shop;
    }

}
