package com.codecampthailand.camp3.java.advanced.day1.day20_HW1;

import com.codecampthailand.camp3.java.advanced.day1.domain.Animal;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * day20_HW1
 */
public class day20_HW1 {

    public static void main(String[] args) {

        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beansAnimal.xml")) {
            Animal animalObj = (Animal) context.getBean("tiger");
            System.out.println(animalObj.getName());

            Animal animalObj2 = (Animal) context.getBean("cat");
            System.out.println(animalObj2.getAnimal());

            Animal animalObj3 = (Animal) context.getBean("petShop");
            System.out.println(animalObj3.getShop());
        }

    }
}