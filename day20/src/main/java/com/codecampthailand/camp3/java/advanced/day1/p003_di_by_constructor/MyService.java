package com.codecampthailand.camp3.java.advanced.day1.p003_di_by_constructor;

public class MyService {
    private MyUtil myUtil;
    private MyUtil myUtil2;
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    // Constructor DI
    public MyService(MyUtil myUtil) {
        this.myUtil = myUtil;
    }

    public MyService(MyUtil myUtil11, MyUtil myUtil22) {
        this.myUtil = myUtil11;
        this.myUtil2 = myUtil22;
    }

    // Setter DI
    public void setMyUtil(MyUtil myUtil) {
        this.myUtil = myUtil;
    }

    public void setMyUtil2(MyUtil myUtil2) {
        this.myUtil2 = myUtil2;
    }

    public void saySomeThing() {
        myUtil.saySomething("I love you");
        myUtil.saySomething("whatToSay");
        myUtil2.saySomething("I hate you");
    }

    public void printThis(String aa) {
        System.out.println(aa);
    }

}