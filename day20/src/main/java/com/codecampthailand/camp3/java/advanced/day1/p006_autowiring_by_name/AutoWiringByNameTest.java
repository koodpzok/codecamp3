package com.codecampthailand.camp3.java.advanced.day1.p006_autowiring_by_name;

import com.codecampthailand.camp3.java.advanced.day1.domain.Employee;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AutoWiringByNameTest {

	public static void main(String[] args) {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans5.xml")) {
			Employee employee = (Employee) context.getBean("employee");
			System.out.println(employee.getAddress());
		}
	}

}