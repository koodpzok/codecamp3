package com.codecampthailand.camp3.java.advanced.day1.domain;

public class Musician {

    private Instrument instrument;
    private Car car;

    public Musician(Instrument instrument) {
        this.instrument = instrument;
    }

    public Musician(Car car) {
        this.car = car;
    }

    public Car getCar() {
        return this.car;
    }

    @Override
    public String toString() {
        return "Musician [instrument=" + instrument + "]";
    }

}