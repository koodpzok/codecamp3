package com.codecampthailand.camp3.java.advanced.day1.p002_singleton;

import com.codecampthailand.camp3.java.advanced.day1.domain.Car;
import com.codecampthailand.camp3.java.advanced.day1.domain.Person;
import com.codecampthailand.camp3.java.advanced.day1.domain.Engine;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SingletonTest {

	public static void main(String[] args) {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans_property.xml")) {
			// try (FileSystemXmlApplicationContext context = new
			// FileSystemXmlApplicationContext(
			// "E:/HW_codecamp/codecamp3/day20/codecamp/src/main/resources/beans_property.xml"))
			// {
			Car car = (Car) context.getBean("car");
			System.out.println(car);
			Car car2 = (Car) context.getBean("car");
			System.out.println(car2);
			Engine engine = (Engine) context.getBean("engine");
			System.out.println(engine);
			Engine engine2 = (Engine) context.getBean("engine");
			System.out.println(engine2);
			System.out.println(car.getEngine().getModel());
			System.out.println(car.getEngine().getHorsePower());

			Person person = (Person) context.getBean("person");
			System.out.println(person.getAge());
			System.out.println(person.getFullName());

			System.out.println(person.getGender());
		}
	}

}