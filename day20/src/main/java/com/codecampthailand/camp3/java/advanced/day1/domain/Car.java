package com.codecampthailand.camp3.java.advanced.day1.domain;

public class Car {

    private String name;

    private Engine engine;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Engine getEngine() {
        return this.engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

}