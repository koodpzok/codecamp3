package day10;

import day10.my_interface.IWebsiteCreator;

/**
 * Cat
 */
public class Cat implements IWebsiteCreator {
    public void createWebsite(String template, String titleName) {
        System.out.println(template + "   " + titleName);
    }

    public void orderWebsite(IWebsiteCreator creator) {
        creator.createWebsite("template", "titleName");
    }
}