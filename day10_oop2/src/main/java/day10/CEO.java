package day10;

import day10.my_interface.ICleaner;
import day10.my_interface.ICoachroachKiller;
import day10.my_interface.IWebsiteCreator;

public class CEO extends Employee {   
    public CEO(){
        super();
    }
    public CEO(String firstnameInput, String lastnameInput, int salaryInput) {
        super(firstnameInput, lastnameInput, salaryInput);
        //super.firstname = firstnameInput;
    }
    @Override
    public int getSalary() {
        return super.getSalary() * 2;
    }
    
    public void hello() {
        System.out.println("Hi, nice to meet you. "+this.firstname+"!");
    }

    public void orderWebsite(IWebsiteCreator creator){
        creator.createWebsite("some templateceo3","codecam4p3");
    }

    public void orderKillCoachroach(ICoachroachKiller person){
        person.killCoachroach(001, "building", "roomName");
        
    }
    // public void killCoachroach() {
    //     setKill(200);
        
    //     //System.out.println("killCoachRoach: " + baygonNumber + " Building: "+ building + " RoomName: "+ roomName);
    // }
    public void orderCleaned(ICleaner obj_Cleaner, String building, String roomName) { // clean is one of functions in ICleaner interface
        obj_Cleaner.clean(building, roomName); //set ICleaner Object ดูจากอันที่callไปลึกสุด
        //ใช้ฟังชั่นใน ICleaner ชื่อว่า clean
        
    }

    // public void orderKillCoachRoach(ICoachroachKiller c) {
    //    // c.buyBaygon(12);
    //     //c.killCoachroach(7, "H11J8","#19");
    //    // System.out.println("Total number of dead cockroaches: " + c.getTotalKilled());

    // }
}
   
   