package day10.my_interface;

public interface IWebsiteCreator {
    public void createWebsite(String template, String titleName);
    public void orderWebsite(IWebsiteCreator creator);
}
   