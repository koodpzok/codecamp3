package day10.my_interface;

import java.util.ArrayList;
import java.util.HashMap;
import day10.Animal;

public interface ICleaner {
    public void setTools(Enum toolName);

    public void clean(String building, String roomName);
    // public String[] getCleanedRoom();
    // public int numCockroach = 0;

    public ArrayList<HashMap<String, String>> getCleanedRoom();
}
