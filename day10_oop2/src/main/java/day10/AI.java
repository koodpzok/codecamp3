package day10;

import java.util.ArrayList;
import java.util.HashMap;
import day10.my_interface.ICleaner;
import day10.my_interface.ICoachroachKiller;
import day10.my_interface.IWebsiteCreator;

public class AI extends Employee implements IWebsiteCreator, ICleaner, ICoachroachKiller {
    public String name;
    public String language;
    private int numCoachRoach = 0;

    public AI(String nameInput, String languageInput) {
        this.name = nameInput;
        this.language = languageInput;
    }

    // IWebsiteCreator
    public void createWebsite(String template, String titleName) {
        System.out.println(language + " automated Setup template: " + template);
        System.out.println(language + " automated Set Title name: " + titleName);
    }

    public void orderWebsite(IWebsiteCreator creator) {
        creator.createWebsite("momo", "titleName");

    }

    // ICleaner interface
    public void setTools(Enum toolName) {
        System.out.println("myTool: " + toolName);
    }

    public void clean(String building, String roomName) {
        // System.out.println("clean building: "+ building + " roomName: "+ roomName);
        // ArrayList<HashMap<String,String>> arrayList= new ArrayList<>();
        HashMap<String, String> myMap = new HashMap<>();
        myMap.put(building, roomName);
        super.arrList.add(myMap);
        // arrayList.add
        // this.getCleanedRoom(building, roomName);

    }

    // public String[] getCleanedRoom() {
    // String[] a = new String[3] ;
    // return a;
    // }

    // Getter
    public ArrayList<HashMap<String, String>> getCleanedRoom() {
        return super.arrList;
    }

    // ICoachroachKiller interface
    public void buyBaygon(int number) {
        System.out.println("buyBaygon: " + number + " ea");
    }

    public void setKill(int kill) {
        this.numCoachRoach += kill;
    }

    public void killCoachroach() {
        setKill(200);

        // System.out.println("killCoachRoach: " + baygonNumber + " Building: "+
        // building + " RoomName: "+ roomName);
    }

    public void killCoachroach(int baygonNumber, String building, String roomName) {
        // System.out.println("killCoachRoach: " + baygonNumber + " Building: "+
        // building + " RoomName: "+ roomName);
        setKill(100);
    }

    public int getTotalKilled() {
        return this.numCoachRoach;
    }
    // @Override
    // public int getTotalKilled() {
    // return super.getTotalKilled();
    // }

}
