package day10;

/**
 * Animal
 */
public class Animal {

    public void consume(Animal ob) {
        ob.print();
    }

    public void print() {
        System.out.println("hello animal");
    }
}