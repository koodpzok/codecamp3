package day10;

import day10.my_interface.IWebsiteCreator;
import day10.my_interface.IWindowsInstaller;

public class Programmer extends Employee implements IWebsiteCreator, IWindowsInstaller {   
    public Programmer(String firstnameInput, String lastnameInput, int salaryInput) {
        super(firstnameInput, lastnameInput, salaryInput);
    }
    public void fixPC(String serialNumber) {
        System.out.println("I'm trying to fix PC serialNumber:" + serialNumber);
    }
    public void createWebsite(String template, String titleName){
        System.out.println("Programmer class: template: " + template + " titleName: " + titleName);
    }
    public void installWindows(String version, String productKey){
        System.out.println(version + "  " + productKey);
    }
    public void formatWindows(String drive){
        System.out.println(drive);
    }
    public int getLastInstalledWindowsVersion(){
        return 10;
    }
    public void orderWebsite(IWebsiteCreator creator){
       // creator.createWebsite("template prommer","codecam4p3");
    }
    // สร้าง Method createWebsite()
    // สร้าง Method installWindows()
}
   
   