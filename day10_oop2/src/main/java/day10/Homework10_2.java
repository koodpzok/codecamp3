package day10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import day10.my_interface.IWebsiteCreator;
import day10.my_interface.ICleaner;

public class Homework10_2 {
    public static void main(String[] args) {
        CEO ceo = new CEO("Captain", "Marvel", 30000);
        Programmer prog = new Programmer("Dang", "Red", 20000);
        OfficeCleaner cleaner = new OfficeCleaner ("Somsri", "Sudsuay", 10000);//this interface ICoachRoachkiller
        AI alphaGo = new AI("alphaGo", "Java");//this interface ICoachRoachkiller
        // ceo.orderKillCoachRoach();
        // ceo.orderKillCoachRoach();
        // ceo.orderKillCoachRoach();
        
        //ceo.killCoachroach();
        cleaner.killCoachroach();//killCoachroach ทีละ 2000
        cleaner.killCoachroach();
        cleaner.killCoachroach();
        ceo.orderKillCoachroach( cleaner );//orderKillCoachroach ทีละ 100
        cleaner.killCoachroach();
        //alphaGo.killCoachroach();
        //System.out.println("Kill : " + cleaner.killCoachroach() + " cockroaches");
        //System.out.println("Kill : " + alphaGo.killCoachroach() + " cockroaches");
        //ceo.orderWebsite(prog);
        //CEO oo = new CEO();
        //System.out.println(ceo.getTotalKilled());
       System.out.println(cleaner.getTotalKilled());

        //System.out.println(alphaGo.getTotalKilled());
        ceo.orderKillCoachroach( cleaner );//orderKillCoachroach ทีละ 100
        ceo.orderKillCoachroach( alphaGo );
        ceo.orderKillCoachroach( alphaGo );
        //alphaGo.killCoachroach();
        System.out.println("cleaner killed "+cleaner.getTotalKilled());
        System.out.println("ai killed " + alphaGo.getTotalKilled());
        ceo.orderCleaned(cleaner, "Building A", "001");
        ceo.orderCleaned(cleaner, "Building B", "002");
        ceo.orderCleaned(cleaner, "Building C", "003");
        ceo.orderCleaned(cleaner, "Building D", "004");
        ceo.orderCleaned(cleaner, "Building E", "005");
        //System.out.println(cleaner.getCleanedRoom());
        ArrayList<HashMap<String, String>> resultByHuman = cleaner.getCleanedRoom(); //OfficeCleaner
        System.out.println(resultByHuman.get(0));
        System.out.println(resultByHuman.get(1));
        System.out.println(resultByHuman.get(2));
        System.out.println(resultByHuman.get(3));
        System.out.println(resultByHuman.get(4));
        

        
        //func(hello.RED);
        alphaGo.setTools(tools.MOP);
        cleaner.setTools(tools.CLEANER_MACHINE);

        ceo.orderCleaned(alphaGo, "Building A", "001");
        ceo.orderCleaned(alphaGo, "Building B", "002");
       // ceo.orderCleaned(cleaner, "Building C", "003");
       // ceo.orderCleaned(cleaner, "Building D", "004");
      //  ceo.orderCleaned(cleaner, "Building E", "005");
        //System.out.println(color_r);
        ArrayList<HashMap<String, String>> resultByAI = alphaGo.getCleanedRoom();// AI
        System.out.println(resultByHuman.get(0));
        System.out.println(resultByAI.get(0));

        //System.out.println(hello.GREEN.getString());
       // for(element k: hello.values())
       // func(hello.values());
    }

    // public static void func(Enum a)
    // {
    //     System.out.println(a.name());
    // }
    public enum hello {
        RED(0,"p"),
        GREEN(4,"green string"),
        BLUE(5,"blue string");

        private int a ;
        private String str;
        private hello(int a, String str)
        {
            this.a = a;
            this.str = str;

        }
        private int getInt(){
            return a;
        }

        private String getString(){
            return str;
        }
    }
  

}   