package day10;

import java.util.ArrayList;
import java.util.HashMap;
import day10.my_interface.ICleaner;
import day10.my_interface.ICoachroachKiller;

public class OfficeCleaner extends Employee implements ICleaner, ICoachroachKiller {
    private int numCoachRoach = 0;
    public void DecorateRoom() {
        System.out.println("DecorateRoom");
    }
    public void WelcomeGuest() {
        System.out.println("WelcomeGuest");
    }
    public OfficeCleaner() {

    }
    public OfficeCleaner(String firstname, String lastname, int salary) {
        super(firstname, lastname, salary);
    }

    //ICleaner interface
    public void setTools(Enum toolName) {
        System.out.println("myTool: " + toolName);
    }
    public void clean(String building, String roomName) {
        //System.out.println("clean building: "+ building + " roomName: "+ roomName);
        //ArrayList<HashMap<String,String>> arrayList= new ArrayList<>();
        HashMap<String, String> myMap = new HashMap<>();
        myMap.put(building, roomName);
        //arrayList.add
       // this.getCleanedRoom(building, roomName);
       super.arrList.add(myMap);
    }

    //Getter
    public ArrayList<HashMap<String,String>> getCleanedRoom() {
        return super.arrList;
    }
    // public String[] getCleanedRoom() {
    //     String[] a = new String[3] ;
    //     return  a;
    // }

    //ICoachroachKiller interface
    public void buyBaygon(int number) {
        System.out.println("buyBaygon: " + number + " ea");
    }
    public void setKill(int kill) {
        this.numCoachRoach += kill;
    }

    public void killCoachroach() {
        setKill(200);
        
        //System.out.println("killCoachRoach: " + baygonNumber + " Building: "+ building + " RoomName: "+ roomName);
    }

    public void killCoachroach(int baygonNumber, String building, String roomName) {
        //System.out.println("killCoachRoach: " + baygonNumber + " Building: "+ building + " RoomName: "+ roomName);
        setKill(100);
    }

    public int getTotalKilled() {
        return this.numCoachRoach;
    }
    // @Override
    // public int getTotalKilled() {
    //     return super.getTotalKilled();
    // }

}