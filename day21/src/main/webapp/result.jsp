<%@ page import="java.util.List" %>
<%@ page import="com.codecampthailand.camp3.java.advanced.day2.domain.Car" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Car You Can Purchase</title>
</head>
<body>
<div style="text-align:center">
    <h4>Hear They Are</h4>
    <ul>
<%  
	@SuppressWarnings("unchecked")
  	List<Car> cars = (List<Car>)request.getAttribute("cars");
  	for (Car car : cars) { 
%>
		<li><%= car.getName() %>, <%= car.getEngine().getModel() %>, <%= car.getEngine().getHorsePower() %> HP</li>
<% 	} %>
    </ul>
</div>
</body>
</html>