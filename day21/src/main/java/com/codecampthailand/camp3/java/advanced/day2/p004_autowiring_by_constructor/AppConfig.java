package com.codecampthailand.camp3.java.advanced.day2.p004_autowiring_by_constructor;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.codecampthailand.camp3.java.advanced.day2.p004_autowiring_by_constructor")

public class AppConfig {
}