package com.codecampthailand.camp3.java.advanced.day2.p005_qualifier;

public interface Car {
    String getCarName();
}