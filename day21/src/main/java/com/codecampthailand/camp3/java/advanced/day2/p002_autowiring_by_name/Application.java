package com.codecampthailand.camp3.java.advanced.day2.p002_autowiring_by_name;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component("application")
public class Application {
    @Resource(name = "applicationUser") // แทนที่ Autowired จะไปหาใน configอย่างเดียว แต่ Resource
                                        // หาจากชื่อcomponentที่เราต้องการ
    private ApplicationUser user;

    public ApplicationUser getUser() {
        return user;
    }

    public void setUser(ApplicationUser user) {
        this.user = user;
    }

}