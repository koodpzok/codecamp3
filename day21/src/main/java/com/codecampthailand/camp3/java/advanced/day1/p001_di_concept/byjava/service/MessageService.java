package com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.service;

public interface MessageService {

	void sendMessage(String msg, String rec);
	
}