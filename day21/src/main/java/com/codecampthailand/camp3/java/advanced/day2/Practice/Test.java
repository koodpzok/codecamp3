package com.codecampthailand.camp3.java.advanced.day2.Practice;

import com.codecampthailand.camp3.java.advanced.day2.domain.Car;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {

    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
            Car c = (Car) context.getBean(Car.class);
            System.out.println(c.getName());
        }

    }

}