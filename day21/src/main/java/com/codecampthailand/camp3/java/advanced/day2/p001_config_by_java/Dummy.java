package com.codecampthailand.camp3.java.advanced.day2.p001_config_by_java;

import org.springframework.stereotype.Component;

@Component
public class Dummy {
    public void init() {
        System.out.println("How do you do?");
    }

    public void close() {
        System.out.println("Good bye!!");
    }

    public void laugh() {
        System.out.println("555");
    }
}