package com.codecampthailand.camp3.java.advanced.day2.p006_autowiring_required_is_false;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AutoWiringRequiredIsFalseTest {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
			Application application = context.getBean(Application.class);
			System.out.println(application.getUser());
		}

	}

}