package com.codecampthailand.camp3.java.advanced.day2.p005_qualifier;

import org.springframework.stereotype.Component;

@Component("Mazda")
public class Mazda implements Car {
    @Override
    public String getCarName() {
        return "Mazda";
    }

}