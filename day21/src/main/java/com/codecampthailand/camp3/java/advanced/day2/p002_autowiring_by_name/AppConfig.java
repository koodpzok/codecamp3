package com.codecampthailand.camp3.java.advanced.day2.p002_autowiring_by_name;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.codecampthailand.camp3.java.advanced.day2.p002_autowiring_by_name")

public class AppConfig {

}