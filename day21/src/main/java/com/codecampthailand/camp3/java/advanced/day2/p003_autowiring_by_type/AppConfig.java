package com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type;

import com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type.domain.Review;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
// @ComponentScan("com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type")
@ComponentScan(basePackages = { "com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type" })
public class AppConfig {

    @Bean(name = "ok")
    public String ok() {
        return "hello";
    }

    // @Bean(name = "ob")
    // public String osk() {
    // return "123";
    // }

    // @Bean
    // public Application app() {
    // return new Application();
    // }

    // @Bean
    // public Review r() {
    // return new Review();
    // }

    // @Bean
    // public ApplicationUser au() {
    // return new ApplicationUser();
    // }
}