package com.codecampthailand.camp3.java.advanced.day2.p002_autowiring_by_name;

import org.springframework.stereotype.Component;

@Component("applicationUser")
public class ApplicationUser {
    private String name = "Code Camp Student";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ApplicationUser [name=" + name + "]";
    }

}