package com.codecampthailand.camp3.java.advanced.day2.p010_inject_collection;

import java.util.List;

import com.codecampthailand.camp3.java.advanced.day2.p006_autowiring_required_is_false.ApplicationUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Application {
    @Autowired
    private ApplicationUser user;

    public ApplicationUser getUser() {
        return user;
    }

    private List<ApplicationUtilities> utilities;

    public void doFunctions() {
        for (ApplicationUtilities utility : utilities) {
            utility.doFunction();
        }
    }

}