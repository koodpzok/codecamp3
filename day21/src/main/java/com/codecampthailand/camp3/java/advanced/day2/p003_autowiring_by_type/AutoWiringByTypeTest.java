package com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AutoWiringByTypeTest {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
			Application application = context.getBean(Application.class);
			System.out.println(application.getUser());
			System.out.println(application.getReview().getName());
			System.out.println(application.ok);
			System.out.println(application.ob);
		}

	}

}