package com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type.domain;

import org.springframework.stereotype.Component;

/**
 * Review
 */

@Component
public class Review {
    private String name = "mimi";

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}