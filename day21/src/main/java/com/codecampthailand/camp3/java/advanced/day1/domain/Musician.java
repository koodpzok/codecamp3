package com.codecampthailand.camp3.java.advanced.day1.domain;

public class Musician {
    
    private Instrument instrument;
     
    public Musician(Instrument instrument){
        this.instrument = instrument;
    }
 
    @Override
    public String toString() {
        return "Musician [instrument=" + instrument + "]";
    }
    
}