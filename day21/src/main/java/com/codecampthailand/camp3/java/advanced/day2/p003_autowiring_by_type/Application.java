package com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type;

import com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type.domain.Review;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Application {
    private ApplicationUser user;
    private Review r;

    @Autowired
    public String ok;

    @Autowired
    public String ob;

    public ApplicationUser getUser() {
        return user;
    }

    public Review getReview() {
        return r;
    }

    @Autowired
    public void setUser(ApplicationUser user) {
        this.user = user;
    }

    @Autowired
    public void setReview(Review r) {
        this.r = r;
    }
}