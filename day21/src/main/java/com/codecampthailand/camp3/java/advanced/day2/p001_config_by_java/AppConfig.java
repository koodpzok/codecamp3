package com.codecampthailand.camp3.java.advanced.day2.p001_config_by_java;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig {
    @Bean(initMethod = "init", destroyMethod = "close")
    @Scope("prototype")
    public Dummy dummy() {
        return new Dummy();
    }

    @Bean
    @Scope("prototype")
    public Person person() {
        return new Person();
    }
}