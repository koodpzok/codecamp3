package com.codecampthailand.camp3.java.advanced.day2.Practice;

import com.codecampthailand.camp3.java.advanced.day2.domain.Car;
import com.codecampthailand.camp3.java.advanced.day2.domain.Engine;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
// @ComponentScan("com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type")

public class AppConfig {
    @Bean
    public Car car() {
        Engine e = new Engine();
        Car c = new Car("Car_name", e);
        return c;
    }
}