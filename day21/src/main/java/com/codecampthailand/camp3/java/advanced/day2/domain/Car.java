package com.codecampthailand.camp3.java.advanced.day2.domain;

import org.springframework.beans.factory.annotation.Autowired;

public class Car {

    private String name;

    private Engine engine;

    public Car(String name, Engine engine) {
        this.name = name;
        this.engine = engine;
    }

    public Car() {

    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Engine getEngine() {
        return this.engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

}