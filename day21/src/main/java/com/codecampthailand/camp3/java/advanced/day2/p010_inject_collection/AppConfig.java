package com.codecampthailand.camp3.java.advanced.day2.p010_inject_collection;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.codecampthailand.camp3.java.advanced.day2.p010_inject_collection")

public class AppConfig {
    @Bean
    List<ApplicationUtilities> applicationUtilities() {
        return Arrays.asList(new DrawUtility(), new CalculationUtility());
    }

}