package com.codecampthailand.camp3.java.advanced.day2.p006_autowiring_required_is_false;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Application {

    private ApplicationUser user;

    public ApplicationUser getUser() {
        return user;
    }

    @Autowired(required = false)
    public void setUser(ApplicationUser user) {
        this.user = user;
    }

}