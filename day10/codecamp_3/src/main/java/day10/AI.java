package day10;

import day10.my_interface.ICleaner;
import day10.my_interface.IWebsiteCreator;

public class AI implements IWebsiteCreator, ICleaner {
    public String name;
    public String language;
    public AI(String nameInput, String languageInput) {
        this.name = nameInput;
        this.language = languageInput;
    }
    public void createWebsite(String template, String titleName) {
        System.out.println(language + " automated Setup template: " + template);
        System.out.println(language + " automated Set Title name: " + titleName);
    }

    //ICleaner interface
    public void setTools(String toolName) {
        System.out.println("myTool: " + toolName);
    }
    public void clean(String building, String roomName) {
        System.out.println("clean building: "+ building + " roomName: "+ roomName);
    }
    public String[] getCleanedRoom() {
        String[] a = new String[3] ;
        return  a;
    }
}
   
   