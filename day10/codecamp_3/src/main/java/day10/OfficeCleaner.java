package day10;

import day10.my_interface.ICleaner;
import day10.my_interface.ICoachroachKiller;

public class OfficeCleaner implements ICleaner, ICoachroachKiller {
    public void DecorateRoom() {
        System.out.println("DecorateRoom");
    }

    public void WelcomeGuest() {
        System.out.println("WelcomeGuest");
    }

    // ICleaner interface
    public void setTools(String toolName) {
        System.out.println("myTool: " + toolName);
    }

    public void clean(String building, String roomName) {
        System.out.println("clean building: " + building + " roomName: " + roomName);
    }

    public String[] getCleanedRoom() {
        String[] a = new String[3];
        return a;
    }

    // ICoachroachKiller interface
    public void buyBaygon(int number) {
        System.out.println("buyBaygon: " + number + " ea");
    }

    public void killCoachroach(int baygonNumber, String building, String roomName) {
        System.out.println("killCoachRoach: " + baygonNumber + " Building: " + building + " RoomName: " + roomName);
    }

    public int getTotalKilled() {
        return 15;
    }

}