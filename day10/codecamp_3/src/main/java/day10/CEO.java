package day10;

import day10.my_interface.ICoachroachKiller;
import day10.my_interface.IWebsiteCreator;

public class CEO extends Employee {   
    public CEO(){
        super();
    }
    public CEO(String firstnameInput, String lastnameInput, int salaryInput) {
        super(firstnameInput, lastnameInput, salaryInput);
        //super.firstname = firstnameInput;
    }
    @Override
    public int getSalary() {
        return super.getSalary() * 2;
    }
    
    public void hello() {
        System.out.println("Hi, nice to meet you. "+this.firstname+"!");
    }

    public void orderWebsite(IWebsiteCreator creator){
        creator.createWebsite("some templateceo3","codecam4p3");
    }

    public void orderKillCoachRoach(ICoachroachKiller c) {
        c.buyBaygon(12);
        c.killCoachroach(7, "H11J8","#19");
        System.out.println("Total number of dead cockroaches: " + c.getTotalKilled());
    }
}
   
   