package myjar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        String[] rawData = { "id:1001 firstname:Luke lastname:Skywalker", "id:1002 firstname:Tony lastname:Stark",
                "id:1003 firstname:Somchai lastname:Jaidee", "id:1004 firstname:Monkey D lastname:Luffee" };

        ArrayList<HashMap<String, String>> myArrayList = new ArrayList<>();

        for (int i = 0; i < rawData.length; i++) {
            String[] split_data = split_String(rawData[i]);

            HashMap<String, String> myMap = new HashMap<>();
            for (int j = 0; j < split_data.length; j++) {
                String[] split_inner = split_data[j].split(":");
                myMap.put(split_inner[0], split_inner[1]);
            }
            myArrayList.add(myMap);
        }

        Iterator<HashMap<String, String>> element = myArrayList.iterator();
        while (element.hasNext()) {
            HashMap<String, String> itr_inner = element.next();//element.next() ก็เหมือน element ใน for(.. : ...)
            //System.out.println("HELLO  "+ itr_inner);
            Iterator<String> k = itr_inner.keySet().iterator();//Obtain an iterator to the start of collection itr_inner.keySet()
            //Iterator<Map.Entry<String,String>> itr_inner2 = itr_inner.entrySet().iterator();
            while (k.hasNext()) {
                //itr_inner is each HashMap object
                //itr_inner.get(kk), find the value whose key is kk
                String kk = k.next();
                //Map.Entry<String,String> kk = itr_inner2.next();
                System.out.println(kk + ": " + itr_inner.get(kk));//get value from HashMap itr_inner using associated key 'kk'
                //System.out.println(kk);
            }

        }

    }

    public static String[] split_String(String input) {
        String[] output = input.split(" ", 2);
        String temp_first_last = output[1];
        int index_s = 0;
        int index_l = 0;
        char[] split_data_char = output[1].toCharArray();
        for (int i = 0; i < split_data_char.length; i++) {
            if (split_data_char[i] == ' ' && split_data_char[i + 1] == 'l' && split_data_char[i + 2] == 'a'
                    && split_data_char[i + 3] == 's' && split_data_char[i + 4] == 't' && split_data_char[i + 5] == 'n'
                    && split_data_char[i + 6] == 'a' && split_data_char[i + 7] == 'm'
                    && split_data_char[i + 8] == 'e') {
                index_s = i;
                index_l = i + 1;
                break;
            }

        }
        String[] output_l = new String[3];
        output_l[0] = output[0];
        output_l[1] = temp_first_last.substring(0, index_s);
        output_l[2] = temp_first_last.substring(index_l);

        return output_l;
    }
}
