package myjar;
import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("Item 1"); 
        arrayList.add("Item 2"); 
        arrayList.add("Item 3");
        Iterator<String> itr = arrayList.iterator();
        int index = 0;

        
        while (itr.hasNext()) {
          String detactVal = itr.next();
          System.out.println("Position: " + (index++) + " Value: " + detactVal);
        }
        

        ListIterator<String> listItr = arrayList.listIterator();

        while(listItr.hasNext()) {
            System.out.println(listItr.next());
           }
           //index 3
           //System.out.println(listItr.previous());
         while (listItr.hasPrevious()) {
             System.out.println(listItr.previous());
            }
            //index -1

            System.out.println('\n');
            HashMap<String, Float> studentScore = new HashMap<>();
            studentScore.put("Somchai", (Float)78.77f);
            studentScore.put("Wirat", (Float)87.22f);
            studentScore.put("Anan", (Float)68.12f);
            studentScore.put("Preecha", (Float)91.32f);
            studentScore.put("Thanarat", (Float)84.08f);
            for (Map.Entry<String,Float> entry : studentScore.entrySet()) {
              System.out.println(entry.getKey() + ": " + entry.getValue() + " score");
            }
            System.out.println('\n');

            ArrayList<String> row_1 = new ArrayList<>();
            row_1.add("Somchai");
            row_1.add("Weerasak");
            ArrayList<String> row_2 = new ArrayList<>();
            row_2.add("Peera");
            row_2.add("Winai");
            row_2.add("Rattana");
            ArrayList<ArrayList<String>> section_a = new ArrayList<>();
            section_a.add(row_1);
            section_a.add(row_2);
            for ( ArrayList<String> row : section_a ) {
                for (String audience : row ) {
                    System.out.println(audience);
                }
                System.out.println();
            
            }
            System.out.println("----------------------------------------");
            HashMap<Integer, String> room_a = new HashMap<>();
            room_a.put(1, "Somchai");
            room_a.put(2, "Somying");

            HashMap<Integer, String> room_b = new HashMap<>();
            room_b.put(1, "Peera");
            room_b.put(2, "Winai");
            room_b.put(3, "Rattana");

            ArrayList<HashMap<Integer, String>> mathClass = new ArrayList<>();
            mathClass.add(room_a);
            mathClass.add(room_b);

            for (HashMap<Integer, String> studentRoom : mathClass) {
                for (String id : studentRoom.values()){
                System.out.println(studentRoom.get(id));
                }
            }


    }
}
