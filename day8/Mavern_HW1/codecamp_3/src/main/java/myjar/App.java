package myjar;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        String[] rawData = {
            "id:1001 firstname:Luke lastname:Skywalker",
            "id:1002 firstname:Tony lastname:Stark",
            "id:1003 firstname:Somchai lastname:Jaidee",
            "id:1004 firstname:Monkey D lastname:Luffee"
          };


            ArrayList<HashMap<String,String>> myArrayList = new ArrayList<HashMap<String,String>>();
            //int count_p=0;
            for(int i = 0; i < rawData.length; i++)
            {
                String[] split_data = split_String(rawData[i]);
                // System.out.println(split_data[0]);//id
                // System.out.println(split_data[1]);//firstname
                // System.out.println(split_data[2]);//lastname
                HashMap<String,String> myMap = new HashMap<String,String>();
                for(int j = 0; j < split_data.length;j++ )
                {
                    
                    String[] split_inner = split_data[j].split(":");
                    myMap.put(split_inner[0], split_inner[1]);
       
                }
                //System.out.println(myMap);
                myArrayList.add(myMap);

            }

            //System.out.println(myArrayList);
            
            //Print all key/values
            for(HashMap<String,String> element: myArrayList)
            {
                //System.out.println(element);
                //System.out.println(element.keySet());
                for(String k: element.keySet())
                {
                    System.out.println(k + ": " + element.get(k));
                }
                //System.out.println(element);
            }
                    
            
        // System.out.println(myMap);
    }

    public static String[] split_String(String input)
    {
        String[] output = input.split(" ",2);
        String temp_first_last = output[1];
        int index_s = 0;
        int index_l = 0;
        char[] split_data_char = output[1].toCharArray();
        for(int i = 0;i < split_data_char.length;i++)
        {
            if(split_data_char[i] == ' ' && split_data_char[i+1] == 'l' && split_data_char[i+2] == 'a' && split_data_char[i+3] == 's' && split_data_char[i+4] == 't' && split_data_char[i+5] == 'n' && split_data_char[i+6] == 'a' && split_data_char[i+7] == 'm' && split_data_char[i+8] == 'e')
            {
                index_s = i;
                index_l = i+1;
                break;
            }

        }
        String[] output_l = new String[3];
        output_l[0] = output[0];
        output_l[1] = temp_first_last.substring(0,index_s);
        output_l[2] = temp_first_last.substring(index_l);
 
        // for(int i = 0; i < output_l.length; i++)
        //     System.out.println(output_l[i]); 

        //     System.out.println(output_l.length);
        return output_l;
    }
}
