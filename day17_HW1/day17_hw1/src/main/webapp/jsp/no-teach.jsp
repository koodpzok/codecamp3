<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>

</head>

<body>

    <table id="myTable" style="border-collapse: collapse">
        <tr>
            <th style="border:1px solid black; text-align: center">id</th>
            <th style="border:1px solid black; text-align: center">instructor name</th>
        </tr>
        <c:forEach items="${NoTeach_Array}" var="element">
            <tr>
                <td style="border:1px solid black; text-align: center">
                    <c:out value="${element.id}" />
                </td>
                <td style="border:1px solid black">
                    <c:out value="${element.name}" />
                </td>

            </tr>
        </c:forEach>

    </table>


</body>

</html>