package com.codecampthailand.camp3.java.advanced.day1.domain;

public class Employee {
	 
    private EmployeeAddress address;
 
    public EmployeeAddress getAddress() {
        return address;
    }
 
    public void setAddress(EmployeeAddress address) {
        this.address = address;
    }

}