package com.codecampthailand.camp3.java.advanced.day2.p010_inject_collection;

public interface ApplicationUtilities {
    void doFunction();
}