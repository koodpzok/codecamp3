package com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.injector;

import com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.consumer.Consumer;

public interface MessageServiceInjector {

	public Consumer getConsumer();
	
}