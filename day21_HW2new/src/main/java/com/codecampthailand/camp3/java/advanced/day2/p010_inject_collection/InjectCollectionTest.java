package com.codecampthailand.camp3.java.advanced.day2.p010_inject_collection;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class InjectCollectionTest {
	
	public static void main(String[] args) {
		try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
            Application application = context.getBean(Application.class);
            System.out.println(application.getUser());
        }

	}

}