package com.codecampthailand.camp3.java.advanced.day21_hw1;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

/**
 * BankAccount
 */
@Component

public class BankAccount {

    private String account_no;
    private int active;
    private BigDecimal amount;

    // private MyDb dbConnection;

    public String getAccount_no() {
        return this.account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public int getActive() {
        return this.active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public BigDecimal getAmount() {
        return this.amount;

    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    // public ArrayList<BankAccount> Insert(String account_no, String active, String
    // amount) {
    // // String sql = "SELECT * FROM employee_users WHERE id = ?";
    // String sql = "INSERT INTO bank_account (account_no,active,amount) VALUES
    // (?,?,?)";
    // String[] bindValues = new String[] { account_no, active, amount };
    // // Use dbConnection
    // ArrayList<BankAccount> employees = dbConnection.employeeQueryGet(sql,
    // bindValues);

    // return employees;
    // }

}