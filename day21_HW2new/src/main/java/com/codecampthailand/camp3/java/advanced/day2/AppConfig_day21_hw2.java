package com.codecampthailand.camp3.java.advanced.day2;

import java.util.ArrayList;
import java.util.List;

import com.codecampthailand.camp3.java.advanced.day2.domain.Car;
import com.codecampthailand.camp3.java.advanced.day2.domain.Engine;
import com.codecampthailand.camp3.java.advanced.day2.service.CarService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig_day21_hw2 {

    @Bean
    public CarService carService() {
        return new CarService();
    }

    @Bean(name = "lowModels")
    public List<Car> setLowModels() {

        List<Car> models = new ArrayList<>();
        Car car = new Car();
        car.setName("Toyota Yaris");
        Engine engine = new Engine();
        engine.setHorsePower(80);
        engine.setModel("1300 cc V4");
        car.setEngine(engine);
        models.add(car);
        return models;
    }

    @Bean(name = "middleModels")
    public List<Car> setMiddleModels() {

        List<Car> models = new ArrayList<>();
        Car car = new Car();
        car.setName("Honda Civic");
        Engine engine = new Engine();
        engine.setHorsePower(150);
        engine.setModel("1800 cc V4");
        car.setEngine(engine);
        models.add(car);
        return models;
    }

    @Bean(name = "highModels")
    public List<Car> setHighModels() {

        List<Car> models = new ArrayList<>();
        Car car = new Car();
        car.setName("BMW 318i");
        Engine engine = new Engine();
        engine.setHorsePower(200);
        engine.setModel("1800 cc V6");
        car.setEngine(engine);
        models.add(car);
        return models;
    }

    @Bean(name = "superModels")
    public List<Car> setSuperModels() {

        List<Car> models = new ArrayList<>();
        Car car = new Car();
        car.setName("Lotus Esprit");
        Engine engine = new Engine();
        engine.setHorsePower(500);
        engine.setModel("5000 cc V12");
        car.setEngine(engine);
        models.add(car);
        return models;
    }
}