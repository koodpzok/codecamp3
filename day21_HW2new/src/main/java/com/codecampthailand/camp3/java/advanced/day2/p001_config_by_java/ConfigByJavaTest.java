package com.codecampthailand.camp3.java.advanced.day2.p001_config_by_java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ConfigByJavaTest {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
			Dummy dummy = context.getBean(Dummy.class);
			Dummy dummy2 = context.getBean(Dummy.class);
			// dummy.laugh();
			Person person = context.getBean(Person.class);
			person.setAge(50);
			System.out.println(person.getAge());
		}
	}

}