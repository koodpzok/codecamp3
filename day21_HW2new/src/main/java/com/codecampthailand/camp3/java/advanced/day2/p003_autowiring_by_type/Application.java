package com.codecampthailand.camp3.java.advanced.day2.p003_autowiring_by_type;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Application {
    private ApplicationUser user;

    public ApplicationUser getUser() {
        return user;
    }

    @Autowired
    public void setUser(ApplicationUser user) {
        this.user = user;
    }

}