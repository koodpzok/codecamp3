package com.codecampthailand.camp3.java.advanced.day21_hw1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Person
 */
@Component
@Scope("prototype")
public class Person {

    private String firstName;
    private String lastName;
    private String gender;
    private BankAccount bankAccount;
    private MyDb dbConnection;

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    // public MyDb getDbConnection() {
    // return this.dbConnection;
    // }

    // @Autowired
    // public void setDbConnection(MyDb dbConnection) {
    // this.dbConnection = dbConnection;
    // }

    @Autowired
    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public BankAccount getBankAccount() {
        return this.bankAccount;
    }

}