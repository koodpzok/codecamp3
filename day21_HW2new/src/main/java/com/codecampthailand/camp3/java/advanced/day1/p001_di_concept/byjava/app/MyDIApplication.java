package com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.app;

import com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.consumer.Consumer;
import com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.service.MessageService;

public class MyDIApplication implements Consumer {

	private MessageService service;

	public MyDIApplication(MessageService svc) {
		this.service = svc;
	}

	@Override
	public void processMessages(String msg, String rec) {
		this.service.sendMessage(msg, rec);
	}

}