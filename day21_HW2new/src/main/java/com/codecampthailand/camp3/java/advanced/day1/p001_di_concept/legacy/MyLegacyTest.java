package com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.legacy;

public class MyLegacyTest {

	public static void main(String[] args) {
		MyApplication app = new MyApplication();
		app.processMessages("Hello", "pb@pb.pb");
	}

}