package com.codecampthailand.camp3.java.advanced.day21_hw1;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.codecampthailand.camp3.java.advanced.day21_hw1")

public class AppConfig {
}