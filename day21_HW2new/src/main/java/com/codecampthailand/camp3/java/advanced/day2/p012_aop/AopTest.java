package com.codecampthailand.camp3.java.advanced.day2.p012_aop;

import java.math.BigDecimal;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AopTest {

    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
            BankAccount bankAccount = context.getBean(BankAccount.class);
            bankAccount.open();
            bankAccount.deposit(BigDecimal.valueOf(10000));
            bankAccount.withdraw(BigDecimal.valueOf(1000));
        }
        System.out.println();

    }

}