package com.codecampthailand.camp3.java.advanced.day2.p012_aop;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class BankAccount {
    private Logger logger = LogManager.getLogger(BankAccount.class);

    public String open() {
        logger.info("Opening the new account");
        return "0000000001";
    }

    public void close() {
        logger.info("Opening the new account");
    }

    public void deposit(BigDecimal amount) {
        logger.info("Deposit " + amount.doubleValue() + " into the account");
    }

    public void withdraw(BigDecimal amount) {
        logger.info("Withdraw " + amount.doubleValue() + " from the account");
    }

    public void changeType(String accountType) {
        logger.info("Account type change to " + accountType);
    }

}