package com.codecampthailand.camp3.java.advanced.day2.p012_aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
    final static Logger logger = LogManager.getLogger(LogAspect.class);

    @Pointcut("execution(* com.codecampthailand.camp3.java.advanced.day2.p012_aop.BankAccount.*(..))")
    private void log() {
    }

    @Before("log()")
    public void beforeAdvice() {
        logger.info("Going to log");
    }

    @After("log()")
    public void afterAdvice() {
        logger.info("Already logged");
    }
}
