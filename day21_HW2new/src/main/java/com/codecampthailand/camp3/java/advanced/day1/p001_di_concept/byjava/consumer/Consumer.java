package com.codecampthailand.camp3.java.advanced.day1.p001_di_concept.byjava.consumer;

public interface Consumer {

	void processMessages(String msg, String rec);
	
}