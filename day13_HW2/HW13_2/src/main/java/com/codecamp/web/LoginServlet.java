package com.codecamp.web;

import com.codecamp.web.Employee;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.fasterxml.jackson.core.type.TypeReference; // พิมพ์ไว้นอก Class
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper; // พิมพ์ไว้นอก Class

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import com.codecamp.web.Student;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // String username = req.getParameter("username");
        // Cookie cookie = new Cookie("username", username);

        // resp.addCookie(cookie);
        // If()
        String username = "";
        String password = "";
        String firstname = "";
        String lastname = "";
        InputStream stream = this.getServletContext().getResourceAsStream("/WEB-INF/single_employee.json");
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        reader.close();

        String jsonContent = stringBuilder.toString();
        ObjectMapper objectMapper = new ObjectMapper();
        // use jackson library
        try {
            HashMap<String, Object> map = objectMapper.readValue(jsonContent,
                    new TypeReference<HashMap<String, Object>>() {
                    });
            // System.out.println(map.get("firstname")); // Captain

            username = (String) map.get("username");
            password = (String) map.get("password");
            firstname = (String) map.get("firstname");
            lastname = (String) map.get("lastname");
            // req.setAttribute("usernamee", map.get("username"));
            // req.setAttribute("password", map.get("password"));
            // if (req.getAttribute("usernamee").equals("dang"))
            // resp.sendRedirect("member");//
            // Cookie c = new Cookie("candy","mlml");

        } catch (IOException ex) {
            // System.out.println("JSON parse error :" + jsonContent + ex.getMessage());
            // req.setAttribute("name", jsonContent);
        }

        // req.setAttribute("message", "");
        if ((req.getParameter("username").equals(username)) && (req.getParameter("password").equals(password))) {

            HttpSession session = req.getSession();
            session.setMaxInactiveInterval(30);

            // all these three variables will be added to sessionScope variable
            session.setAttribute("firstname", firstname);
            session.setAttribute("lastname", lastname);
            session.setAttribute("loginmessage", "You have signed-in as " + firstname + " " + lastname);
            // req.getRequestDispatcher("/jsp/member.jsp").forward(req, resp);
            resp.sendRedirect("member");//
        } else {
            req.setAttribute("message", "username/password is incorrect.");
            req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);// เอาattribute ส่งไปหน้าถัดไป
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // req.setAttribute("name", jsonContent);

        req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
    }

}
