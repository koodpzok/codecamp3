package com.codecamp.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee {
    private int id;
    private String firstname;
    private String lastname;
    private String company;
    private int salary;

    // private String gender;

    public Employee() {
        this(0, "Anonymous", "Anonymous", "XXXX", 0);
        // this("XXXXfirstname","XXXXXlastname","Male",444);
    }

    public Employee(int id, String firstname, String lastname, String company, int salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.company = company;
        this.salary = salary;
    }
    // public Employee(int id, String firstname, String lastname, int salary)
    // {
    // this.id = id;
    // this.firstname = firstname;
    // this.lastname = lastname;
    // this.salary = salary;
    // }
    // public Employee(String firstname, String lastname,String gender, int salary)
    // {
    // this.firstname = firstname;
    // this.lastname = lastname;
    // this.gender = gender;
    // this.salary = salary;
    // }

    // Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    // Getters
    public int getId() {
        return this.id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public String getCompany() {
        return this.company;
    }

    public int getSalary() {
        return this.salary;
    }

    // ห้ามใส่ชื่อฟังชั่นขึ้นต้นว่า get เพราะ jackson library จะดึงไปเกี่่ยว
    // public String getFullName(){
    // String fullname = "";
    // if(gender.toLowerCase().equals("male"))
    // fullname += "Mr. "+this.firstname + " " + this.lastname;
    // else if(gender.toLowerCase().equals("female"))
    // fullname += "Mrs. "+this.firstname + " " + this.lastname;

    // return fullname;
    // }

    // public int getDoubleSalary() {
    // return this.salary*2 ;
    // }

}