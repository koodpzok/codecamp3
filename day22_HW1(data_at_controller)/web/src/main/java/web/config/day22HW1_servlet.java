package web.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import web.exception.SpringException;

//dispatcherServlet config
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "web" })
public class day22HW1_servlet implements WebMvcConfigurer {

  @Bean
  public InternalResourceViewResolver resolver() {
    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
    resolver.setViewClass(JstlView.class);
    resolver.setPrefix("/");
    resolver.setSuffix(".jsp");
    return resolver;
  }

  @Bean
  public HandlerExceptionResolver errorHandler() {
    SimpleMappingExceptionResolver s = new SimpleMappingExceptionResolver();
    Properties p = new Properties();
    p.setProperty(SpringException.class.getName(), "springExceptionView");
    // SpringException.class.getName() มี valueเป็น springExceptionView.jsp
    s.setExceptionMappings(p);
    s.setDefaultStatusCode(400);
    return s;
  }

  // @Bean
  // public ViewResolver beanNameResolver() {
  // return new BeanNameViewResolver();
  // }
  // same as above
  @Override
  public void configureViewResolvers(ViewResolverRegistry registry) {
    registry.beanName();
  }

  @Bean("jsonView")
  public View jsonView() {
    MappingJackson2JsonView view = new MappingJackson2JsonView();
    view.setPrettyPrint(true);
    return view;
  }

  @Bean("personView")
  public View personView() {
    MappingJackson2JsonView view = new MappingJackson2JsonView();
    view.setPrettyPrint(true);
    return view;
  }
}
