package com.company;

import com.company.model.*;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.jdbc.Driver;

public class CourseServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        // ArrayList<Course> courses = getAllUsers();
        // ---------------------------------------------

        MyDb myDb = MyDb.getInstance();
        Course courseObj = new Course(myDb);
        // courseObj.SetDbConnection(myDb);

        String id = req.getParameter("id");
        ArrayList<Course> courses_id = courseObj.courseIdGet(id);
        String price = req.getParameter("price");
        ArrayList<Course> courses_price = courseObj.coursePriceGet(price);

        String jsonOutput = "";
        try {
            if (id == null) {

            } else {
                jsonOutput = objectMapper.writeValueAsString(courses_id);
            }

            if (price == null) {

            } else {
                jsonOutput = objectMapper.writeValueAsString(courses_price);
            }
            // System.out.println(jsonOutput);
        } catch (JsonProcessingException ex) {
            System.out.println("There's something wrong with writeValueAsString: " + ex.getMessage());
        }

        ServletOutputStream outputStream = resp.getOutputStream();
        outputStream.print(jsonOutput);
        outputStream.flush();
        outputStream.close();
    }

    // protected ArrayList<Course> getUserById(String idFromGet) {
    // ArrayList<Course> courses = new ArrayList<>();
    // try {
    // DriverManager.registerDriver(new Driver());
    // Connection connection =
    // DriverManager.getConnection("jdbc:mysql://localhost:3306/design_pattern",
    // "root",
    // "lnwsuper");
    // PreparedStatement statement = connection.prepareStatement("SELECT * FROM
    // courses WHERE id = ?");

    // // Bind values into the parameters.
    // statement.setString(1, idFromGet); // เซ็ตค่า idFromGet ไปแทนที่ ? ที่เว้นไว้
    // ใน SQL Statement
    // ResultSet resultSet = statement.executeQuery();

    // while (resultSet.next()) {
    // Course course = new Course(resultSet);

    // courses.add(course);
    // }
    // statement.close();
    // connection.close();
    // } catch (SQLException e) {
    // e.printStackTrace();
    // }

    // return courses;
    // }

    // protected ArrayList<Course> getUserByPrice(String priceFromGet) {
    // ArrayList<Course> courses = new ArrayList<>();
    // try {
    // DriverManager.registerDriver(new Driver());
    // Connection connection =
    // DriverManager.getConnection("jdbc:mysql://localhost:3306/design_pattern",
    // "root",
    // "lnwsuper");
    // PreparedStatement statement = connection.prepareStatement("SELECT * FROM
    // courses WHERE price = ?");

    // // Bind values into the parameters.
    // statement.setString(1, priceFromGet); // เซ็ตค่า idFromGet ไปแทนที่ ?
    // ที่เว้นไว้ ใน SQL Statement
    // ResultSet resultSet = statement.executeQuery();

    // while (resultSet.next()) {
    // Course course = new Course(resultSet);

    // courses.add(course);
    // }
    // statement.close();
    // connection.close();
    // } catch (SQLException e) {
    // e.printStackTrace();
    // }

    // return courses;
    // }
}
