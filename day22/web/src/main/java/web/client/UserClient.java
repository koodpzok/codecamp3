package web.client;

import org.springframework.web.client.RestTemplate;

/**
 * UserClient
 */
public class UserClient {
    public static void main(String[] args) {
        // String uri = "https://jsonplaceholder.typicode.com/todos/1";
        String uri = "http://localhost:8081/camp3/employees";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);

        System.out.println(result);
    }

}