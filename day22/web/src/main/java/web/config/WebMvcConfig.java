package web.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import web.controller.EmployeeController;
import web.exception.SpringException;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "web" })
public class WebMvcConfig implements WebMvcConfigurer {

    @Bean
    // สร้าง object ที่เปลี่ยน view.jsp เป็น bean
    public ViewResolver beanNameResolver() {
        return new BeanNameViewResolver();
    }

    @Bean
    public InternalResourceViewResolver resolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public HandlerExceptionResolver errorHandler() {
        SimpleMappingExceptionResolver s = new SimpleMappingExceptionResolver();
        Properties p = new Properties();
        p.setProperty(SpringException.class.getName(), "springExceptionView");
        s.setExceptionMappings(p);
        s.setDefaultStatusCode(400);
        return s;
    }

    // ชื่อbeanที่ใช้แทน .jsp
    @Bean("jsonView")
    public View jsonView() {
        MappingJackson2JsonView view = new MappingJackson2JsonView();
        view.setPrettyPrint(true);

        return view;
    }

}
