package web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

// org.springframework.web.servlet.DispatcherServlet
import web.domain.Employee;

@Controller
public class EmployeeController {

    // @RequestMapping(value = "/employees", produces =
    // MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    // @RequestMapping(value = "/employees", produces = { "application/json" },
    // method = RequestMethod.GET)

    @RequestMapping(value = { "/employees",
            "/emp" }, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public String getAllEmployeesJSON(Model model) {
        model.addAttribute("employees", getEmployeesCollection());
        return "jsonView";// มองหาbeanชื่อjsonView หรือ jspชื่อjsonView
    }

    // @RequestMapping(value = "/employees", produces =
    // MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    // @ResponseBody
    // public List<Employee> getAllEmployeesJSON(Model model) {
    // //model.addAttribute("employees", getEmployeesCollection());
    // return getEmployeesCollection();
    // }

    private List<Employee> getEmployeesCollection() {
        List<Employee> employees = new ArrayList<>();
        Employee max = new Employee(1, "Pakawat", "Boonyoung", "max@gmail.com");
        Employee mee = new Employee(2, "Mee", "Boonyoung", "mee@yahoo.com");
        Employee panda = new Employee(3, "Panda", "Boonyoung", "panda@gmail.com");
        employees.add(max);
        employees.add(mee);
        employees.add(panda);
        return employees;
    }

}
