package web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * HelloController
 */
// @Controller
// @RequestMapping("/greet")
// public class HelloController {
// @RequestMapping(method = RequestMethod.GET)
// public String greeting(ModelMap model) {
// model.addAttribute("message", "Welcome to Spring Web");
// return "hello";// jsp file
// }
// }

@Controller
// @RequestMapping("/greet")
public class HelloController {
    @RequestMapping(value = "/greet", method = RequestMethod.GET)
    public String greeting(ModelMap model) {
        model.addAttribute("message", "Welcome to Spring Web");
        return "hello";// jsp file
    }
}