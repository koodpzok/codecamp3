package com.company.model;

import com.company.MyDb;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Course {
    private int id;
    private String name;
    private String detail;
    private int price;
    private int teach_by;
    private MyDb dbConnection;

    public Course() {

    }

    public Course(ResultSet resultSet) {
        try {
            this.id = resultSet.getInt("id");
            this.name = resultSet.getString("name");
            this.detail = resultSet.getString("detail");
            this.price = resultSet.getInt("price");
            this.teach_by = resultSet.getInt("teach_by");
        } catch (SQLException ex) {
            System.out.println("Cannot new Course Object. " + ex.toString());

        }
    }

    public Course(MyDb dbConnection) {
        this.dbConnection = dbConnection; // Constructor Injection
    }

    public void SetDbConnection(MyDb newConnection) {
        this.dbConnection = newConnection; // Property Injection
    }

    public ArrayList<Course> courseIdGet(String id) {
        String sql = "SELECT * FROM courses WHERE id = ?";
        String[] bindValues = new String[] { id };
        // Use dbConnection

        ArrayList<Course> courses = dbConnection.courseQueryGet(sql, bindValues);

        return courses;
    }

    public ArrayList<Course> coursePriceGet(String price) {
        String sql = "SELECT * FROM courses WHERE price = ?";
        String[] bindValues = new String[] { price };
        // Use dbConnection

        ArrayList<Course> courses = dbConnection.courseQueryGet(sql, bindValues);

        return courses;
    }

    public MyDb GetDbConnection() {
        return dbConnection;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return this.detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getTeach_by() {
        return this.teach_by;
    }

    public void setTeach_by(int teach_by) {
        this.teach_by = teach_by;
    }

}