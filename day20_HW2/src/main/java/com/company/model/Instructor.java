package com.company.model;

import com.company.MyDb;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Instructor {
    private int id;
    private String name;

    private MyDb dbConnection;

    public Instructor() {

    }

    public Instructor(ResultSet resultSet) {
        try {
            this.id = resultSet.getInt("id");
            this.name = resultSet.getString("name");

        } catch (SQLException ex) {
            System.out.println("Cannot new Instructor Object. " + ex.toString());

        }
    }

    public Instructor(MyDb dbConnection) {
        this.dbConnection = dbConnection; // Constructor Injection
    }

    public void SetDbConnection(MyDb newConnection) {
        this.dbConnection = newConnection; // Property Injection
    }

    public ArrayList<Instructor> instructorGetAllUsers() {
        String sql = "SELECT * FROM instructors";
        String[] bindValues = new String[] {};
        // Use dbConnection

        ArrayList<Instructor> instructors = dbConnection.instructorQueryGet(sql, bindValues);

        return instructors;
    }

    public ArrayList<Instructor> instructorIdGet(String id) {
        String sql = "SELECT * FROM instructors WHERE id = ?";
        String[] bindValues = new String[] { id };
        // Use dbConnection

        ArrayList<Instructor> instructors = dbConnection.instructorQueryGet(sql, bindValues);

        return instructors;
    }

    public MyDb GetDbConnection() {
        return dbConnection;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}