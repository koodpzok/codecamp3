package com.company;

import com.company.domain.Engine;
import com.company.model.Course;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mysql.jdbc.Driver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * HW
 */

public class HWServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml")) {
            // Engine ee = (Engine) context.getBean("engine");// call id 'myService' from
            // beans2.xml
            // System.out.println(ee.getModel());
            Course course = (Course) context.getBean("course");

            String id = req.getParameter("id");
            ArrayList<Course> courses_id = course.courseIdGet(id);
            String price = req.getParameter("price");
            ArrayList<Course> courses_price = course.coursePriceGet(price);
            // ArrayList<Course> courses_id = courseObj.courseIdGet(id);
            // courses_price.get(0).getPrice();

            String jsonOutput = "";
            try {
                if (id == null) {

                } else {
                    jsonOutput = objectMapper.writeValueAsString(courses_id);
                    jsonOutput = jsonOutput.replace("[", "");
                    jsonOutput = jsonOutput.replace("]", "");
                }

                if (price == null) {

                } else {
                    jsonOutput = objectMapper.writeValueAsString(courses_price);
                    jsonOutput = jsonOutput.replace("[", "");
                    jsonOutput = jsonOutput.replace("]", "");
                }
            } catch (JsonProcessingException ex) {
                System.out.println("There's something wrong with writeValueAsString: " + ex.getMessage());
            }

            ServletOutputStream outputStream = resp.getOutputStream();

            outputStream.print(jsonOutput);
            outputStream.flush();
            outputStream.close();

        }

    }
}
