package com.company;

import com.company.model.*;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.jdbc.Driver;

public class InstructorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        MyDb myDb = MyDb.getInstance();
        Instructor instObj = new Instructor(myDb);
        // instObj.SetDbConnection(myDb);
        ArrayList<Instructor> instructors = instObj.instructorGetAllUsers();
        // ---------------------------------------------
        String id = req.getParameter("id");

        // ArrayList<Instructor> instructors_id = getUserById(id);

        ArrayList<Instructor> instructors_id = instObj.instructorIdGet(id);
        String jsonOutput = "";
        try {
            if (id == null) {
                jsonOutput = objectMapper.writeValueAsString(instructors);
            } else {
                jsonOutput = objectMapper.writeValueAsString(instructors_id);
            }
            // System.out.println(jsonOutput);
        } catch (JsonProcessingException ex) {
            System.out.println("There's something wrong with writeValueAsString: " + ex.getMessage());
        }

        ServletOutputStream outputStream = resp.getOutputStream();
        outputStream.print(jsonOutput);
        outputStream.flush();
        outputStream.close();
    }

    // protected ArrayList<Instructor> getAllUsers() {
    // ArrayList<Instructor> instructors = new ArrayList<>();
    // try {
    // DriverManager.registerDriver(new Driver());
    // Connection connection =
    // DriverManager.getConnection("jdbc:mysql://localhost:3306/design_pattern",
    // "root",
    // "lnwsuper");
    // PreparedStatement statement = connection.prepareStatement("SELECT * FROM
    // instructors");

    // // Bind values into the parameters.
    // // statement.setString(1, idFromGet); // เซ็ตค่า idFromGet ไปแทนที่ ?
    // ที่เว้นไว้
    // // ใน SQL Statement
    // ResultSet resultSet = statement.executeQuery();

    // while (resultSet.next()) {

    // Instructor instructor = new Instructor(resultSet);
    // instructors.add(instructor);
    // }
    // statement.close();
    // connection.close();
    // } catch (SQLException e) {
    // e.printStackTrace();
    // }

    // return instructors;
    // }

    // protected ArrayList<Instructor> getUserById(String idFromGet) {
    // ArrayList<Instructor> instructors = new ArrayList<>();
    // try {
    // DriverManager.registerDriver(new Driver());
    // Connection connection =
    // DriverManager.getConnection("jdbc:mysql://localhost:3306/design_pattern",
    // "root",
    // "lnwsuper");
    // PreparedStatement statement = connection.prepareStatement("SELECT * FROM
    // instructors WHERE id = ?");

    // // Bind values into the parameters.
    // statement.setString(1, idFromGet); // เซ็ตค่า idFromGet ไปแทนที่ ? ที่เว้นไว้
    // ใน SQL Statement
    // ResultSet resultSet = statement.executeQuery();

    // while (resultSet.next()) {
    // Instructor instructor = new Instructor(resultSet);

    // instructors.add(instructor);
    // }
    // statement.close();
    // connection.close();
    // } catch (SQLException e) {
    // e.printStackTrace();
    // }
    // MyDb myDb = MyDb.getInstance();
    // String sql = "SELECT * FROM instructors WHERE id = ?";
    // String[] bindValues = new String[] { idFromGet };
    // ArrayList<Instructor> instructors = myDb.instructorQueryGet(sql, bindValues);
    // return instructors;
    // }
}
