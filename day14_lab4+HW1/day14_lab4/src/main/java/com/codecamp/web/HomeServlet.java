package com.codecamp.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class HomeServlet extends HttpServlet {
    // final String KEY_PAGE_VISIT = "pageVisit";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/codecamp", "root",
                    "lnwsuper");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM student");
            // System.out.println(resultSet);
            ArrayList<Student> students = new ArrayList<Student>();
            while (resultSet.next()) {
                Student student = new Student();
                String studentId = resultSet.getString("studentId");
                String firstname = resultSet.getString("firstname");
                String lastname = resultSet.getString("lastname");

                student.setStudentId(studentId);
                student.setFirstName(firstname);
                student.setLastName(lastname);
                students.add(student);
                System.out.println("ID=" + studentId + ", Firstname=" + firstname + ", Lastname=" + lastname);
                // System.out.println(String.format("ID=%s, Firstname=%s, Lastname=%s",
                // studentId, firstname, lastname));
            }
            req.setAttribute("Student_Array", students);
            // System.out.println(students.size());
            req.getRequestDispatcher("/jsp/home.jsp").forward(req, resp);
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
