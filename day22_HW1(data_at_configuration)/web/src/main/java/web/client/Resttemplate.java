package web.client;

import org.springframework.web.client.RestTemplate;

/**
 * client
 */
// RestTemplate
public class Resttemplate {

    public static void main(String[] args) {
        String uri = "https://jsonplaceholder.typicode.com/todos/1";
        RestTemplate restTemplate = new RestTemplate();
        // restTemplate.postForObject(url, request, responseType, uriVariables)
        String result = restTemplate.getForObject(uri, String.class);
        System.out.println(result);
    }

}