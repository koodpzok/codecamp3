package web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import web.domain.Person;
import web.exception.SpringException;

/**
 * PersonController
 */
@Controller
public class PersonController {
    @Autowired
    List<Person> persons;

    @RequestMapping(value = "/register_form", method = RequestMethod.GET)
    public ModelAndView getShowRegisterForm() {

        return new ModelAndView("person", "registerModel", new Person());
    }

    @RequestMapping(value = "/register_form", method = RequestMethod.POST)
    public String addtoPersonsList(@ModelAttribute("registerModel") Person p, Model model)
    // public String addPlusRedirect(HttpServletResponse httpServletResponse,
    // @ModelAttribute("registerModel") Person p)
    {

        Boolean isReplicate = false;
        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getEmail().equals(p.getEmail())) {
                isReplicate = true;
            }
        }

        if (!isReplicate) {
            if (p.getFirstName().isEmpty() || p.getLastName().isEmpty() || p.getEmail().isEmpty())
                throw new SpringException("some of the attributes r empty!!");

            Person pObj = new Person();
            pObj.setFirstName(p.getFirstName());
            pObj.setLastName(p.getLastName());
            pObj.setEmail(p.getEmail());
            persons.add(pObj);
            // ----------------------------------------------- view
            // ObjectMapper objmp = new ObjectMapper();
            // String toShow = objmp.writeValueAsString(persons);
            // httpServletResponse.sendRedirect("http://localhost:8081/day22_HW1/register_form");
            // return toShow;
            model.addAttribute("registerModel", persons);
            return "personView";// render view from personView bean
        } else {
            // httpServletResponse.sendRedirect("http://localhost:8081/day22_HW1/register_form");//
            // redirect
            model.addAttribute("validationError", "Please Enter");
            return "person";// render view from person.jsp
        }

    }

}