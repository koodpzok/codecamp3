<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>

<head>
    <title>Registration Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        h2,
        span,
        input {
            margin-left: 30px;
        }
    </style>
</head>

<body>
    <!-- ${command.getName()} -->
    <!-- path is the property in student, name id age -->
    <!-- this will use setter for all properties written here -->

    <div style="width:50%;margin: auto;margin-top: 50px;border:1px solid black;background-color:rgb(131, 165, 240)">

        <form:form method="POST" action="/day22_HW1_atConfig/register_form" modelAttribute="registerModel">

            <table>
                <h2 style="padding-bottom: 50px;padding-top: 50px;color:green">Registration Form </h2>
                <tr>
                    <td>
                        <span>Name</span>
                    </td>
                    <td>
                        <form:input path="firstName" style="width: 120px;margin-right: 10px;margin-left:100px" />
                        <form:input path="lastName" style="width: 180px" />

                    </td>
                    <td>
                        <span style="color: red">
                            ${validationError}
                        </span>

                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td>
                        <span style="color:gray;margin-left:100px">
                            First Name
                        </span>
                        <span style="padding-left:55px;color: gray">
                            Last Name
                        </span>
                    </td>


                </tr>

                <tr>
                    <td>
                        <span>Email</span>
                    </td>
                    <td>
                        <form:input type="email" path="email" style="margin-left:100px;width: 300px" />
                    </td>
                </tr>


                <tr>
                    <td colspan="2" style="text-align: center">
                        <input type="submit" value="Submit" style="margin-top:25px;" />
                    </td>
                </tr>
            </table>
        </form:form>

    </div>
</body>

</html>