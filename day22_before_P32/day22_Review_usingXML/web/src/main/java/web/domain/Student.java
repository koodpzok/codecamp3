package web.domain;

public class Student {

    private Integer age;
    private String nameE;
    private Integer id;

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public void setNameE(String name) {
        this.nameE = name;
    }

    public String getNameE() {
        return nameE;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
