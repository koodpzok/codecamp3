package web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import web.domain.Student;

@Controller
public class StudentController {
    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public ModelAndView student() {
        // Student a = new Student();
        // a.setName("gge");
        // return new ModelAndView("student", "command", new Student());
        return new ModelAndView("student", "studentZ", new Student());
        // สร้างstudent objectเปล่าๆเก็บไว้ในstudentZ แล้วเอาไปโชว์ในstudent.jsp

    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    // public String addStudent(@ModelAttribute("SpringWeb") Student student,
    // ModelMap model) {
    public String addStudent(@ModelAttribute("studentZ") Student student, ModelMap model) {
        // we already set the student name age id from browser now we r getting it to
        // show on result.jsp
        model.addAttribute("name", student.getNameE());
        model.addAttribute("age", student.getAge());
        model.addAttribute("id", student.getId());
        return "result";
    }
}
