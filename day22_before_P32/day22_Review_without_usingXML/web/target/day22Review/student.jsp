<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>

<head>
    <title>Student Registration Form</title>
</head>

<body>
    <!-- ${command.getName()} -->
    <!-- path is the property in student, name id age -->
    <!-- this will use setter for all properties written here -->
    <h2>Student Information </h2>
    <form:form method="POST" action="/day22Review/register" modelAttribute="studentZ">
        <table>
            <tr>
                <td>
                    <form:label path="nameE">Name</form:label>
                </td>
                <td>
                    <form:input path="nameE" />
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="age">Age</form:label>
                </td>
                <td>
                    <form:input path="age" />
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="id">id</form:label>
                </td>
                <td>
                    <form:input path="id" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Submit" />
                </td>
            </tr>
        </table>
    </form:form>
</body>

</html>