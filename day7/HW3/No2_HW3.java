public class No2_HW3 {
    public static void main(String[] args)
    {
        draw10(4);
    }

    public static void draw10(int n)
    {
        int p = 2;
        for(int i = 0; i < n; i++)
        {
            System.out.println(p);
            p += 2;
        }
    }
}