public class No6_HW3 {
    public static void main(String[] args)
    {
        draw14(4);
    }

    public static void draw14(int n)
    {
        int sInd = 1;
        for(int i = 0; i<n;i++)
        {
            //print *
            for(int k = 0; k<= sInd-1; k++)
            {
                System.out.print('*');
            }

            //print _
            for(int j = sInd; j<n; j++)
            {
                System.out.print('_');
                
            }
            sInd += 1;
            System.out.println();
        }
    }
}