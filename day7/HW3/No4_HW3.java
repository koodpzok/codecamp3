public class No4_HW3 {
    public static void main(String[] args) {
        draw12(3);
    }
    public static void draw12(int n){
        for(int i = 0;i < n; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if (i!=j)
                {
                    System.out.print('*');
                }
                else
                    System.out.print('_');
            }
            System.out.println();
        }
    }

}