public class HW3 {
    public static void main(String[] args)
    {
        draw9(3);
        // draw10(3);
        // draw11(3);
        // draw12(3);
        // draw13(3);
        // draw14(3);
        // draw15(3);
        // draw16(3);
    }

    public static void draw9(int n)
    {
        int p = 0;
        for(int i = 0; i < n; i++)
        {
            System.out.println(p);
            p += 2;
        }
    }

    public static void draw10(int n)
    {
        int p = 2;
        for(int i = 0; i < n; i++)
        {
            System.out.println(p);
            p += 2;
        }
    }

    public static void draw11(int n)
    {
        for(int i = 1; i <= n; i++)
        {
            int temp = i;
            for(int j = 0;j < n; j++)
            {
                
                System.out.print(temp);
                temp += i;
            }
            System.out.println();
        }

    }

    public static void draw12(int n){
        for(int i = 0;i < n; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if (i!=j)
                {
                    System.out.print('*');
                }
                else
                    System.out.print('_');
            }
            System.out.println();
        }
    }

    public static void draw13(int n){
        for(int i = 0;i < n; i++)
        {
            for(int j = n-1; j >= 0; j--)
            {
                if (i!=j)
                {
                    System.out.print('*');
                }
                else
                    System.out.print('_');
            }
            System.out.println();
        }
    }

    public static void draw14(int n)
    {
        int sInd = 1;
        for(int i = 0; i<n;i++)
        {
            //print *
            for(int k = 0; k<= sInd-1; k++)
            {
                System.out.print('*');
            }

            //print _
            for(int j = sInd; j<n; j++)
            {
                System.out.print('_');
                
            }
            sInd += 1;
            System.out.println();
        }
    }

    public static void draw15(int n)
    {
        int sInd = n;
        for(int i = 0; i<n;i++)
        {
            //print *
            for(int k = 0; k<= sInd-1; k++)
            {
                System.out.print('*');
            }

            //print _
            for(int j = sInd; j<n; j++)
            {
                System.out.print('_');
                
            }
            sInd -= 1;
            System.out.println();
        }
    }

    public static void draw16(int n)
    {
        int sInd = 1;
        
        for (int i = 0; i < n+(n-1); i++)//no of rows
        {
            if (i < n-1)
            {
                
                 //print *
                for(int k = 0; k<= sInd-1; k++)
                {
                    System.out.print('*');
                }

                //print _
                for(int j = sInd; j<n; j++)
                {
                    System.out.print('_');
                    
                }
                sInd += 1;
            }
            else if (i > n-1)
            {
                    //print *
                for(int k = 0; k< sInd-1; k++)
                {
                    System.out.print('*');
                }

                //print _
                for(int j = sInd-1; j<n; j++)
                {
                    System.out.print('_');
                    
                }
                sInd -= 1;
            }
            else {//print middle i == n-1
                
                for(int k=0;k < n;k++)
                {
                    System.out.print('*');
                }
            }

            System.out.println();
        }
    }

}