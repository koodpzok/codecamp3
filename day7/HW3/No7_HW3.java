public class No7_HW3 {
    public static void main(String[] args)
    {
        draw15(2);
    }

    public static void draw15(int n)
    {
        int sInd = n;
        for(int i = 0; i<n;i++)
        {
            //print *
            for(int k = 0; k<= sInd-1; k++)
            {
                System.out.print('*');
            }

            //print _
            for(int j = sInd; j<n; j++)
            {
                System.out.print('_');
                
            }
            sInd -= 1;
            System.out.println();
        }
    }
}