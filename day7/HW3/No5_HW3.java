public class No5_HW3 {
    public static void main(String[] args)
    {
        draw13(4);
    }

    public static void draw13(int n){
        for(int i = 0;i < n; i++)
        {
            for(int j = n-1; j >= 0; j--)
            {
                if (i!=j)
                {
                    System.out.print('*');
                }
                else
                    System.out.print('_');
            }
            System.out.println();
        }
    }
}