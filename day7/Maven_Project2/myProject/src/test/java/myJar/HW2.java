package myJar;

public class HW2 {
    public static void main (String[] args)
    {
        String[][] table = { 
            { "1", "2", "3" }, 
            { "4", "5", "6" }, 
            { "7", "8", "9" }
       }; 
        multiplyTable(table);       
        
    }

    public static void multiplyTable(String[][] table)
    {
        
        for(int set=0; set < table.length;set++)
        {
            
            //for(String element: table[set])
            for(int j=0; j < table[set].length; j++)
            {
                //int temp = Integer.parseInt(element) * 2;
                int temp = Integer.parseInt(table[set][j]) * 2;
                System.out.print(temp);
                if(j < table[set].length-1)
                    System.out.print(", ");
            }
            System.out.println();
        }

        
    }
}