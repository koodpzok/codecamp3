public class No7 {
    public static void main(String[] args) {
        draw7(3);
    }

    public static void draw7(int n) {
        for (int i = 1; i <= n * n; i++) {
            System.out.print(i);
            if ((i % n) == 0)
                System.out.println();
        }
    }
}