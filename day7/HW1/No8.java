public class No8 {
    public static void main(String[] args)
    {
        draw8(4);
    }
    public static void draw8(int n)
    {
        int count = 0;
        for(int i=n*n;i>=1;i--)
        {
            System.out.print(i);
            count++;
            if(count == n)
            {
                count = 0;
                System.out.println();
            }
            
        }
    }
}