import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;

class ExamPloy {
    public static void main(String[] args) {
        String[] rawData = { "id:1001 firstname:Luke lastname:Skywalker", "id:1002 firstname:Tony lastname:Stark",
                "id:1003 firstname:Somchai lastname:Jaidee", "id:1004 firstname:MonkeyD lastname:Luffee" };

        ArrayList<HashMap<String, String>> AvengersArrList = new ArrayList<>();
        for (String key : rawData) {
            // System.out.println(key);
            String[] splitItem = key.split(" ");
            HashMap<String, String> hash = new HashMap<>();
            for (String item : splitItem) {
                String[] split2 = item.split(":");
                // System.out.println(split2[0]);
                hash.put(split2[0], split2[1]);
            }
            AvengersArrList.add(hash);

            // for (HashMap<String, String> hashKey : AvengersArrList) {

            Iterator<Map.Entry<String, String>> entrySet = hash.entrySet().iterator();
            while (entrySet.hasNext()) {
                Map.Entry<String, String> entry = entrySet.next();
                System.out.println(entry.getKey() + " : " + entry.getValue());
            }
            // }
        }
    }

    // System.out.println(AvengersArrList);

}
