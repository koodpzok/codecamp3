package myJar;
public class HW1 {
    public static void main(String[] args)
    {
        for(int n = 2; n < 5; n++)
        {
            draw8(n);
            System.out.println();
        }
        
    }

    public static void draw1(int n)
    {
        for(int i = 0; i < n;i++)
            System.out.print('*');
    }

    public static void draw2(int n)
    {
        for(int i = 0;i<n;i++)
        {
            for(int j=0;j<n;j++)
            {
                System.out.print('*');
            }
            System.out.print('\n');
        }

    }

    public static void draw3(int n)
    {

        for(int i=1;i<=n;i++)
        {
            for(int j=1;j<=n;j++)
            {
                System.out.print(j);
            }
            System.out.print("\n");
        }
    }

    public static void draw4(int n){
        for(int i = 0;i<n;i++)
        {
            for(int j=n; j>=1; j--)
            {
                System.out.print(j);
            }
            System.out.print("\n");
        }
    }

    public static void draw5 (int n)
    {
        for(int i = 1; i <= n; i++)
        {
            for(int j = 0; j<n; j++)
            {
                System.out.print(i);
            }
            System.out.print("\n");
        }
    }

    public static void draw6(int n)
    {
        for(int i = n; i >= 1; i--)
        {
            for(int j = 0; j < n ; j++)
            {
                System.out.print(i);
            }
            System.out.println();
        }
    }

    public static void draw7(int n)
    {
        for(int i = 1; i <= n*n; i++)
        {
            System.out.print(i);
            if((i % n) == 0)
                System.out.println();
        }
    }

    public static void draw8(int n)
    {
        int count = 0;
        for(int i=n*n;i>=1;i--)
        {
            System.out.print(i);
            count++;
            if(count == n)
            {
                count = 0;
                System.out.println();
            }
            
        }
    }
}