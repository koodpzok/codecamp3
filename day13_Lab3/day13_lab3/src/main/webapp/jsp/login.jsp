<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
</head>

<body>
    <form method="POST" action="login">
        <p>
            <label>Username: </label>
            <input type="text" name="username" />
        </p>
        <p>
            <label>Password: </label>
            <input type="password" name="password" />
        </p>
        <p>
            <button type="submit">Login</button>
        </p>
    </form>
</body>

</html>