package myjar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference; // พิมพ์ไว้นอก Class
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Lab3 {

    public static void main(String[] agrs) {
        //P.1
        ObjectMapper objectMapper = new ObjectMapper();
        Employee dang = new Employee(1001, "Dang", "Red", 10000);
        //System.out.println(dang.getFirstname() + "  " +dang.getLastname());
        // try {
        //     //Java Object to JSON File
        //     objectMapper.writeValue(new File("target/employee.json"), dang);
        // } 
        // catch (IOException ex) {
        //     System.out.println("There's something wrong with target/employee.json " + 
        //     ex.getMessage());
        // }


        //P.2  Java Object to JSON String, dang is Java Object
        try {
            String employeeAsString = objectMapper.writeValueAsString(dang);
           // System.out.println(dang.getClass().getName());
            System.out.println(employeeAsString);
         // {"id":1001,"firstname":"Dang","lastname":"Red","salary":10000}
         } catch (JsonProcessingException ex) {
         System.out.println("There's something wrong with writeValueAsString: " + 
         ex.getMessage());
         }

         //P.3 JSON File to Java Object
         try{
            Employee dang2 = objectMapper.readValue(new File("target/employee.json"), Employee.class);
             System.out.println(dang2.getFirstname()); // Dang
         } catch (IOException ex) {
            System.out.println("JSON parse to Employee object error " + ex.getMessage());
         }
         
         //P.4 JSON URL to Java Object
        //  try{
        //     Employee dang2 = objectMapper.readValue(new URL("file:target/employee.json"), Employee.class);
        //      System.out.println(dang2.getFirstname()); // Dang
        //  } catch (IOException ex) {
        //     System.out.println("JSON parse to Employee object error " + ex.getMessage());
        //  }

        //P.5 JSON String to Java Object
        // String json = "{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 }";
        // try {
        //     Employee dang2 = objectMapper.readValue(json, Employee.class);;           
        //     System.out.println(dang2.getId());
        // } catch (IOException ex) {
        //     System.out.println("JSON parse error: " + json + ex.getMessage());
        // }

        //P.6 Java List from a JSON Array String
        String jsonEmployeeArray = "[{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 },{ \"id\" : 1002, \"firstname\" : \"Captain2\", \"lastname\" : \"Marvel2\", \"salary\" : 30000 }]";
        try{
            List<Employee> employees = objectMapper.readValue(jsonEmployeeArray, new TypeReference<List<Employee>>(){});
            String firstname = employees.get(1).getFirstname();
            System.out.println(firstname);
        } catch (IOException ex) {
            System.out.println("JSON parse error: " + jsonEmployeeArray + ex.getMessage());
        }

        //P.7 Java Map from JSON String
        String json2 = "{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 }";
        try{
           HashMap<String, Object> map = objectMapper.readValue(json2, new TypeReference<HashMap<String,Object>>(){});
           System.out.println(map.get("firstname")); // Captain
           System.out.println(map);
        } catch (IOException ex) {
           System.out.println("JSON parse error :" + json2 + ex.getMessage());
        }
        
        //P.8 Java Array from a JSON Array String
        String jsonEmployeeArray2 = "[{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 },{ \"id\" : 1002, \"firstname\" : \"Captain2\", \"lastname\" : \"Marvel2\", \"salary\" : 30000 }]";
        try{
            objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
            Employee[] employees = objectMapper.readValue(jsonEmployeeArray2, Employee[].class);
            System.out.println(employees[1].getFirstname()); // Captain2
        } catch (IOException ex) {
            System.out.println("JSON parse error " + jsonEmployeeArray2 + ex.getMessage());
        }

        //P.9 Java ArrayList of HashMap from a JSON Array String
        String jsonEmployeeArray3 = "[{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 },{ \"id\" : 1002, \"firstname\" : \"Captain2\", \"lastname\" : \"Marvel2\", \"salary\" : 30000 }]";
        try{
            ArrayList<HashMap<String, String>> employees = objectMapper.readValue(jsonEmployeeArray3, new 
            TypeReference<ArrayList<HashMap<String, String>>>(){});
            String firstname = employees.get(1).get("id"); // 1002
            System.out.println(firstname);
            //System.out.println(employees.get(0));
        } catch (IOException ex) {
            System.out.println("JSON parse error: " + jsonEmployeeArray3 + ex.getMessage());
        }

         //P.11 JSON to Jackson JsonNode
         try{
                String json3 = "{ \"name\" : \"Dang\", \"innerObject\" : { \"keyInside2\" : { \"keyInside3\" : 123 } } }";
                JsonNode jsonNode = objectMapper.readTree(json3);
                String name = jsonNode.get("name").textValue();
                System.out.println(name); // Dang
                int valueInside = jsonNode.get("innerObject").get("keyInside2").get("keyInside3").intValue();
                System.out.println(valueInside); // 123
        } catch (IOException ex) {
                System.out.println("JSON parse to Employee object error " + ex.getMessage());
        }
         
    }
}