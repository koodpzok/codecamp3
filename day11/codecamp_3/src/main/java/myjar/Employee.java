package myjar;

import myjar.my_interface.Itest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee implements Itest {
    private String firstname;
    private String lastname;
    private String gender;
    private int salary;
    private int id;

    public Employee(){
       // this("XXXXfirstname","XXXXXlastname","Male",444);
    }
    public Employee(int id, String firstname, String lastname, int salary)
    {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
        //this.gender = gender;
    }
    // public Employee(String firstname, String lastname,String gender, int salary)
    // {
    //     this.firstname = firstname;
    //     this.lastname = lastname;
    //     this.gender = gender;
    //     this.salary = salary;
    // }

    //Setters
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public void setId(int id) {
        this.id = id;
    }

    //Getters
    public String getFirstname() {
        return this.firstname;
    }
    public String getLastname() {
        return this.lastname;
    }
    public int getSalary() {
        return this.salary;
    }
    public int getId() {
        return this.id;
    }

    //ห้ามใส่ชื่อฟังชั่นขึ้นต้นว่า get เพราะ jackson library จะดึงไปเกี่่ยว
    // public String getFullName(){
    //     String fullname = "";
    //     if(gender.toLowerCase().equals("male"))
    //         fullname += "Mr. "+this.firstname + " " + this.lastname;
    //     else if(gender.toLowerCase().equals("female"))
    //         fullname += "Mrs. "+this.firstname + " " + this.lastname;

    //     return fullname;
    // }

    // public int getDoubleSalary() {
    //     return this.salary*2 ;
    // }

}