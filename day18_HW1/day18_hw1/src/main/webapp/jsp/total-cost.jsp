<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>

</head>

<body>

    <table id="myTable" style="border-collapse: collapse">
        <tr>
            <th style="border:1px solid black; text-align: center">student id</th>
            <th style="border:1px solid black; text-align: center">course id</th>
            <th style="border:1px solid black; text-align: center">price</th>
        </tr>
        <c:forEach items="${Enroll_array}" var="element">
            <tr>
                <td style="border:1px solid black; text-align: center">
                    <c:out value="${element.getStudent_id()}" />
                </td>
                <td style="border:1px solid black; text-align: center">
                    <c:out value="${element.getCourse_id()}" />
                </td>
                <td style="border:1px solid black; text-align: center">
                    <c:out value="${element.getPrice()}" />
                </td>

            </tr>
        </c:forEach>
    </table>


</body>

</html>