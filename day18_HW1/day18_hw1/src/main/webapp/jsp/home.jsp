<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList" %>
<html>

<head>

</head>

<body>

    <table id="myTable" style="border-collapse: collapse">
        <tr>
            <th style="border:1px solid black; text-align: center">Id</th>
            <th style="border:1px solid black; text-align: center">First name</th>
            <th style="border:1px solid black; text-align: center">Last name</th>
            <th style="border:1px solid black; text-align: center">Age</th>
        </tr>
        <c:forEach items="${Employee_Array}" var="element">
            <tr>
                <td style="border:1px solid black; text-align: center">
                    <c:out value="${element.id}" />
                </td>
                <td style="border:1px solid black">
                    <c:out value="${element.firstname}" />
                </td>
                <td style="border:1px solid black">
                    <c:out value="${element.lastname}" />
                </td>
                <td style="border:1px solid black">
                    <c:out value="${element.age}" />
                </td>
            </tr>
        </c:forEach>

    </table>

    <table id="myTable2" style="margin-top:15px; border-collapse: collapse">
        <tr>
            <th style="border:1px solid black; text-align: center">ISBN</th>
            <th style="border:1px solid black; text-align: center">Title</th>
            <th style="border:1px solid black; text-align: center">Price</th>

        </tr>
        <c:forEach items="${Book_Array}" var="element">
            <tr>
                <td style="border:1px solid black; text-align: center">
                    <c:out value="${element.ISBN}" />
                </td>
                <td style="border:1px solid black">
                    <c:out value="${element.title}" />
                </td>
                <td style="border:1px solid black">
                    <c:out value="${element.price}" />
                </td>

            </tr>
        </c:forEach>

    </table>

</body>

</html>