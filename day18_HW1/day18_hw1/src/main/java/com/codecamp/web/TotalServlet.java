package com.codecamp.web;

import com.codecamp.web.Enrolls;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class TotalServlet extends HttpServlet {
    // final String KEY_PAGE_VISIT = "pageVisit";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "lnwsuper");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select sum(price) from enrolls inner join courses on enrolls.course_id = courses.id;");
            // System.out.println(resultSet);
            // ArrayList<Person> persons = new ArrayList<Person>();
            String sumPrice = "";
            while (resultSet.next()) {
                // Person person = new Person();
                // String id = resultSet.getString("id");
                // String name = resultSet.getString("name");
                // String firstname = resultSet.getString("firstname");
                // String lastname = resultSet.getString("lastname");
                // String age = resultSet.getString("age");

                // person.setId(id);
                // person.setName(name);

                // persons.add(person);
                // System.out.println("ID=" + id + ", name=" + name);
                // System.out.println(String.format("ID=%s, Firstname=%s, Lastname=%s",
                // studentId, firstname, lastname));
                sumPrice = resultSet.getString("sum(price)");

            }

            req.setAttribute("sum_Price", sumPrice);
            // System.out.println(students.size());
            req.getRequestDispatcher("/jsp/total.jsp").forward(req, resp);
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
