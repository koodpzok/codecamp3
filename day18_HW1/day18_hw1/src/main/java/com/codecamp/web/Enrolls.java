package com.codecamp.web;

public class Enrolls {
    private String student_id;
    private String course_id;
    private String price;

    public Enrolls() {

    }

    // Getters
    public String getStudent_id() {
        return this.student_id;
    }

    public String getCourse_id() {
        return this.course_id;
    }

    public String getPrice() {
        return this.price;
    }

    // Setters
    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}